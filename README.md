tournament
==========

Sistema de gestión de torneos, por ahora de fútbol

A Symfony project created on April 15, 2015, 9:45 am.



Algunos comandos utiles

Creación de entidades de cero
php app/console generate:doctrine:entity --no-interaction  --format=annotation --entity=TournamentBundle:Round --fields="number:string(10) estimatedDate:datetime matches:array"
php app/console generate:doctrine:entity --no-interaction  --format=annotation --entity=TournamentBundle:Match --fields="local:integer visitor:integer round:integer orderInRound:integer court:integer refereee:integer dateAndTime:datetime refereeObservations:text status:string(15)"

Actualización de las entidades (generar getters y setters luego de agregar variables)
php app/console doctrine:generate:entities Bundle/TournamentBundle/Entity/Team

Actualizacion de la BD
php app/console doctrine:schema:update --force

Sonata - generar Admin class para una Entity
php app/console sonata:admin:generate Bundle/TournamentBundle/Entity/Team
