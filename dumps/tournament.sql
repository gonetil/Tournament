-- MySQL dump 10.13  Distrib 5.6.25, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: tournament
-- ------------------------------------------------------
-- Server version	5.6.25-0ubuntu0.15.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `acl_classes`
--

DROP TABLE IF EXISTS `acl_classes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_classes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class_type` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_69DD750638A36066` (`class_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acl_classes`
--

LOCK TABLES `acl_classes` WRITE;
/*!40000 ALTER TABLE `acl_classes` DISABLE KEYS */;
/*!40000 ALTER TABLE `acl_classes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acl_entries`
--

DROP TABLE IF EXISTS `acl_entries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_entries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class_id` int(10) unsigned NOT NULL,
  `object_identity_id` int(10) unsigned DEFAULT NULL,
  `security_identity_id` int(10) unsigned NOT NULL,
  `field_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ace_order` smallint(5) unsigned NOT NULL,
  `mask` int(11) NOT NULL,
  `granting` tinyint(1) NOT NULL,
  `granting_strategy` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `audit_success` tinyint(1) NOT NULL,
  `audit_failure` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_46C8B806EA000B103D9AB4A64DEF17BCE4289BF4` (`class_id`,`object_identity_id`,`field_name`,`ace_order`),
  KEY `IDX_46C8B806EA000B103D9AB4A6DF9183C9` (`class_id`,`object_identity_id`,`security_identity_id`),
  KEY `IDX_46C8B806EA000B10` (`class_id`),
  KEY `IDX_46C8B8063D9AB4A6` (`object_identity_id`),
  KEY `IDX_46C8B806DF9183C9` (`security_identity_id`),
  CONSTRAINT `FK_46C8B8063D9AB4A6` FOREIGN KEY (`object_identity_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_46C8B806DF9183C9` FOREIGN KEY (`security_identity_id`) REFERENCES `acl_security_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_46C8B806EA000B10` FOREIGN KEY (`class_id`) REFERENCES `acl_classes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acl_entries`
--

LOCK TABLES `acl_entries` WRITE;
/*!40000 ALTER TABLE `acl_entries` DISABLE KEYS */;
/*!40000 ALTER TABLE `acl_entries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acl_object_identities`
--

DROP TABLE IF EXISTS `acl_object_identities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_object_identities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_object_identity_id` int(10) unsigned DEFAULT NULL,
  `class_id` int(10) unsigned NOT NULL,
  `object_identifier` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `entries_inheriting` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_9407E5494B12AD6EA000B10` (`object_identifier`,`class_id`),
  KEY `IDX_9407E54977FA751A` (`parent_object_identity_id`),
  CONSTRAINT `FK_9407E54977FA751A` FOREIGN KEY (`parent_object_identity_id`) REFERENCES `acl_object_identities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acl_object_identities`
--

LOCK TABLES `acl_object_identities` WRITE;
/*!40000 ALTER TABLE `acl_object_identities` DISABLE KEYS */;
/*!40000 ALTER TABLE `acl_object_identities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acl_object_identity_ancestors`
--

DROP TABLE IF EXISTS `acl_object_identity_ancestors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_object_identity_ancestors` (
  `object_identity_id` int(10) unsigned NOT NULL,
  `ancestor_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`object_identity_id`,`ancestor_id`),
  KEY `IDX_825DE2993D9AB4A6` (`object_identity_id`),
  KEY `IDX_825DE299C671CEA1` (`ancestor_id`),
  CONSTRAINT `FK_825DE2993D9AB4A6` FOREIGN KEY (`object_identity_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_825DE299C671CEA1` FOREIGN KEY (`ancestor_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acl_object_identity_ancestors`
--

LOCK TABLES `acl_object_identity_ancestors` WRITE;
/*!40000 ALTER TABLE `acl_object_identity_ancestors` DISABLE KEYS */;
/*!40000 ALTER TABLE `acl_object_identity_ancestors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acl_security_identities`
--

DROP TABLE IF EXISTS `acl_security_identities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_security_identities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `username` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8835EE78772E836AF85E0677` (`identifier`,`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acl_security_identities`
--

LOCK TABLES `acl_security_identities` WRITE;
/*!40000 ALTER TABLE `acl_security_identities` DISABLE KEYS */;
/*!40000 ALTER TABLE `acl_security_identities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `court`
--

DROP TABLE IF EXISTS `court`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `court` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `court`
--

LOCK TABLES `court` WRITE;
/*!40000 ALTER TABLE `court` DISABLE KEYS */;
INSERT INTO `court` VALUES (1,'Bombonera',1),(2,'Monumental',1),(3,'Estadio Único',1);
/*!40000 ALTER TABLE `court` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `football_match`
--

DROP TABLE IF EXISTS `football_match`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `football_match` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `local_id` int(11) DEFAULT NULL,
  `visitor_id` int(11) DEFAULT NULL,
  `round_id` int(11) DEFAULT NULL,
  `court_id` int(11) DEFAULT NULL,
  `referee_id` int(11) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  `local_participation` int(11) DEFAULT NULL,
  `visitor_participation` int(11) DEFAULT NULL,
  `played_football_match` int(11) DEFAULT NULL,
  `orderInRound` int(11) NOT NULL,
  `dateAndTime` datetime DEFAULT NULL,
  `refereeObservations` longtext COLLATE utf8_unicode_ci,
  `visitor_goals` int(11) NOT NULL,
  `local_goals` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8CE33ACE25DA0831` (`local_participation`),
  UNIQUE KEY `UNIQ_8CE33ACE576FE3D9` (`visitor_participation`),
  UNIQUE KEY `UNIQ_8CE33ACEF58CFA5D` (`played_football_match`),
  KEY `IDX_8CE33ACE5D5A2101` (`local_id`),
  KEY `IDX_8CE33ACE70BEE6D` (`visitor_id`),
  KEY `IDX_8CE33ACEA6005CA0` (`round_id`),
  KEY `IDX_8CE33ACEE3184009` (`court_id`),
  KEY `IDX_8CE33ACE4A087CA2` (`referee_id`),
  KEY `IDX_8CE33ACE6BF700BD` (`status_id`),
  CONSTRAINT `FK_8CE33ACE25DA0831` FOREIGN KEY (`local_participation`) REFERENCES `team_match_participation` (`id`),
  CONSTRAINT `FK_8CE33ACE4A087CA2` FOREIGN KEY (`referee_id`) REFERENCES `referee` (`id`),
  CONSTRAINT `FK_8CE33ACE576FE3D9` FOREIGN KEY (`visitor_participation`) REFERENCES `team_match_participation` (`id`),
  CONSTRAINT `FK_8CE33ACE5D5A2101` FOREIGN KEY (`local_id`) REFERENCES `team_in_tournament` (`id`),
  CONSTRAINT `FK_8CE33ACE6BF700BD` FOREIGN KEY (`status_id`) REFERENCES `football_match_status` (`id`),
  CONSTRAINT `FK_8CE33ACE70BEE6D` FOREIGN KEY (`visitor_id`) REFERENCES `team_in_tournament` (`id`),
  CONSTRAINT `FK_8CE33ACEA6005CA0` FOREIGN KEY (`round_id`) REFERENCES `round` (`id`),
  CONSTRAINT `FK_8CE33ACEE3184009` FOREIGN KEY (`court_id`) REFERENCES `court` (`id`),
  CONSTRAINT `FK_8CE33ACEF58CFA5D` FOREIGN KEY (`played_football_match`) REFERENCES `played_football_match` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `football_match`
--

LOCK TABLES `football_match` WRITE;
/*!40000 ALTER TABLE `football_match` DISABLE KEYS */;
INSERT INTO `football_match` VALUES (1,2,5,1,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,-1,-1),(2,3,4,1,NULL,NULL,NULL,NULL,NULL,NULL,2,NULL,NULL,-1,-1),(3,5,1,2,NULL,NULL,2,1,2,1,0,NULL,NULL,-1,-1),(4,3,2,2,NULL,NULL,NULL,NULL,NULL,NULL,2,NULL,NULL,-1,-1),(5,1,4,3,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,-1,-1),(6,5,3,3,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,-1,-1),(7,3,1,4,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,-1,-1),(8,2,4,4,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,-1,-1),(9,1,2,5,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,-1,-1),(10,4,5,5,NULL,NULL,NULL,NULL,NULL,NULL,2,NULL,NULL,-1,-1);
/*!40000 ALTER TABLE `football_match` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `football_match_status`
--

DROP TABLE IF EXISTS `football_match_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `football_match_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `football_match_status`
--

LOCK TABLES `football_match_status` WRITE;
/*!40000 ALTER TABLE `football_match_status` DISABLE KEYS */;
INSERT INTO `football_match_status` VALUES (1,'preparado',''),(2,'finalizado',''),(3,'suspendido',''),(4,'cancelado',''),(5,'creado','');
/*!40000 ALTER TABLE `football_match_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fos_user_group`
--

DROP TABLE IF EXISTS `fos_user_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fos_user_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_583D1F3E5E237E06` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fos_user_group`
--

LOCK TABLES `fos_user_group` WRITE;
/*!40000 ALTER TABLE `fos_user_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `fos_user_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fos_user_user`
--

DROP TABLE IF EXISTS `fos_user_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fos_user_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `managed_team` int(11) DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expired` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `credentials_expired` tinyint(1) NOT NULL,
  `credentials_expire_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `date_of_birth` datetime DEFAULT NULL,
  `firstname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `biography` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `locale` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timezone` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json)',
  `twitter_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter_data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json)',
  `gplus_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gplus_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gplus_data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json)',
  `token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `two_step_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_C560D76192FC23A8` (`username_canonical`),
  UNIQUE KEY `UNIQ_C560D761A0D96FBF` (`email_canonical`),
  KEY `IDX_C560D7611766E5E0` (`managed_team`),
  CONSTRAINT `FK_C560D7611766E5E0` FOREIGN KEY (`managed_team`) REFERENCES `team` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fos_user_user`
--

LOCK TABLES `fos_user_user` WRITE;
/*!40000 ALTER TABLE `fos_user_user` DISABLE KEYS */;
INSERT INTO `fos_user_user` VALUES (1,NULL,'superadmin','superadmin','contacto@superadmin.com.ar','contacto@superadmin.com.ar',1,'katxugvvnlwg8s48gg4kgwwg8cgwg0k','tODR3gJU5X4/P1oLrLi8s04/zb60Q8Pv3IHHQ5baYwU8i/9AhClj7R2283po/rpn738a5GUfu/1dJp/h24/VSg==','2015-09-03 22:46:23',0,0,NULL,NULL,NULL,'a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}',0,NULL,'2015-07-15 23:37:19','2015-09-03 22:46:23',NULL,NULL,NULL,NULL,NULL,'u',NULL,NULL,NULL,NULL,NULL,'null',NULL,NULL,'null',NULL,NULL,'null',NULL,NULL),(2,NULL,'delegate','delegate','contacto@delegate.com','contacto@delegate.com',1,'2wd3p0121u68k40s880080kgcsk0c4c','GITlzb95U4iZ/QgvsphI6xr1mUqljmj2SAKBombkVtyxA0jH6n5IFd4+UXcKF0W/ZSuvc4SabhzQJLOFTF7OVw==','2015-09-09 20:29:51',0,0,NULL,NULL,NULL,'a:1:{i:0;s:13:\"ROLE_DELEGATE\";}',0,NULL,'2015-09-06 20:06:58','2015-09-09 20:29:51',NULL,NULL,NULL,NULL,NULL,'u',NULL,NULL,NULL,NULL,NULL,'null',NULL,NULL,'null',NULL,NULL,'null',NULL,NULL),(3,NULL,'admin','admin','contacto@admin.com','contacto@admin.com',1,'qm8450166qogc4k0o0440gocgg8w44k','/eB4rm/6oeingTl8ZOTQXSu22RquPXfwonbaX4c9DKU7qXcIGDZRv+MojJk9tri34ivm7jYJ6E0IpibvoWop0A==','2015-09-29 18:22:25',0,0,NULL,NULL,NULL,'a:1:{i:0;s:10:\"ROLE_ADMIN\";}',0,NULL,'2015-09-06 20:07:41','2015-09-29 18:22:25',NULL,NULL,NULL,NULL,NULL,'u',NULL,NULL,NULL,NULL,NULL,'null',NULL,NULL,'null',NULL,NULL,'null',NULL,NULL);
/*!40000 ALTER TABLE `fos_user_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fos_user_user_group`
--

DROP TABLE IF EXISTS `fos_user_user_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fos_user_user_group` (
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`group_id`),
  KEY `IDX_B3C77447A76ED395` (`user_id`),
  KEY `IDX_B3C77447FE54D947` (`group_id`),
  CONSTRAINT `FK_B3C77447A76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user_user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_B3C77447FE54D947` FOREIGN KEY (`group_id`) REFERENCES `fos_user_group` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fos_user_user_group`
--

LOCK TABLES `fos_user_user_group` WRITE;
/*!40000 ALTER TABLE `fos_user_user_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `fos_user_user_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment`
--

DROP TABLE IF EXISTS `payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `team_in_tournament` int(11) DEFAULT NULL,
  `payment_reason` int(11) DEFAULT NULL,
  `date` date NOT NULL,
  `amount` double NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_6D28840D8780F5FE` (`team_in_tournament`),
  KEY `IDX_6D28840D1E80139A` (`payment_reason`),
  CONSTRAINT `FK_6D28840D1E80139A` FOREIGN KEY (`payment_reason`) REFERENCES `payment_reason` (`id`),
  CONSTRAINT `FK_6D28840D8780F5FE` FOREIGN KEY (`team_in_tournament`) REFERENCES `team_in_tournament` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment`
--

LOCK TABLES `payment` WRITE;
/*!40000 ALTER TABLE `payment` DISABLE KEYS */;
/*!40000 ALTER TABLE `payment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment_reason`
--

DROP TABLE IF EXISTS `payment_reason`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_reason` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment_reason`
--

LOCK TABLES `payment_reason` WRITE;
/*!40000 ALTER TABLE `payment_reason` DISABLE KEYS */;
/*!40000 ALTER TABLE `payment_reason` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `played_football_match`
--

DROP TABLE IF EXISTS `played_football_match`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `played_football_match` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `football_match` int(11) DEFAULT NULL,
  `local_participation` int(11) DEFAULT NULL,
  `visitor_participation` int(11) DEFAULT NULL,
  `dateAndTime` datetime DEFAULT NULL,
  `court` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `referee` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `refereeObservations` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_F58CFA5D8CE33ACE` (`football_match`),
  UNIQUE KEY `UNIQ_F58CFA5D25DA0831` (`local_participation`),
  UNIQUE KEY `UNIQ_F58CFA5D576FE3D9` (`visitor_participation`),
  CONSTRAINT `FK_F58CFA5D25DA0831` FOREIGN KEY (`local_participation`) REFERENCES `team_match_participation` (`id`),
  CONSTRAINT `FK_F58CFA5D576FE3D9` FOREIGN KEY (`visitor_participation`) REFERENCES `team_match_participation` (`id`),
  CONSTRAINT `FK_F58CFA5D8CE33ACE` FOREIGN KEY (`football_match`) REFERENCES `football_match` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `played_football_match`
--

LOCK TABLES `played_football_match` WRITE;
/*!40000 ALTER TABLE `played_football_match` DISABLE KEYS */;
INSERT INTO `played_football_match` VALUES (1,NULL,1,2,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `played_football_match` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `player`
--

DROP TABLE IF EXISTS `player`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `player` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `teamInTournament_id` int(11) DEFAULT NULL,
  `lastname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `firstname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `dni` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `birth` datetime NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `healthInfo` longtext COLLATE utf8_unicode_ci,
  `number` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_98197A65F8C387B4` (`teamInTournament_id`),
  CONSTRAINT `FK_98197A65F8C387B4` FOREIGN KEY (`teamInTournament_id`) REFERENCES `team_in_tournament` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `player`
--

LOCK TABLES `player` WRITE;
/*!40000 ALTER TABLE `player` DISABLE KEYS */;
INSERT INTO `player` VALUES (1,1,'Villareal','Gonzalo','28671089','1925-01-01 00:00:00','gonetil@gmail.com','en forma',11),(2,NULL,'Lira','Ariel','29992332','1983-04-01 00:00:00','arieljlira@gmail.com',NULL,10),(3,NULL,'Gianotti','Jorge Oscar','33218971','1987-08-05 00:00:00','jorge.oscar.gianotti@gmail.com',NULL,14);
/*!40000 ALTER TABLE `player` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `player_participation`
--

DROP TABLE IF EXISTS `player_participation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `player_participation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `player` int(11) DEFAULT NULL,
  `team_match_participation` int(11) DEFAULT NULL,
  `yellow_cards` int(11) DEFAULT NULL,
  `red_cards` int(11) DEFAULT NULL,
  `goals` int(11) DEFAULT NULL,
  `suspended_dates` int(11) DEFAULT NULL,
  `tshirt_number` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_7EB59EFB98197A65` (`player`),
  KEY `IDX_7EB59EFBCD04CF77` (`team_match_participation`),
  CONSTRAINT `FK_7EB59EFB98197A65` FOREIGN KEY (`player`) REFERENCES `player` (`id`),
  CONSTRAINT `FK_7EB59EFBCD04CF77` FOREIGN KEY (`team_match_participation`) REFERENCES `team_match_participation` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `player_participation`
--

LOCK TABLES `player_participation` WRITE;
/*!40000 ALTER TABLE `player_participation` DISABLE KEYS */;
INSERT INTO `player_participation` VALUES (1,1,2,1,0,0,0,NULL);
/*!40000 ALTER TABLE `player_participation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `referee`
--

DROP TABLE IF EXISTS `referee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `referee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `referee`
--

LOCK TABLES `referee` WRITE;
/*!40000 ALTER TABLE `referee` DISABLE KEYS */;
INSERT INTO `referee` VALUES (1,'Labruna',1),(2,'Castrillo',1),(3,'Nimo',1);
/*!40000 ALTER TABLE `referee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `round`
--

DROP TABLE IF EXISTS `round`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `round` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tournament` int(11) NOT NULL,
  `round_status` int(11) DEFAULT NULL,
  `number` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `estimatedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_C5EEEA34BD5FB8D9` (`tournament`),
  KEY `IDX_C5EEEA34BFB75CB0` (`round_status`),
  CONSTRAINT `FK_C5EEEA34BD5FB8D9` FOREIGN KEY (`tournament`) REFERENCES `tournament` (`id`),
  CONSTRAINT `FK_C5EEEA34BFB75CB0` FOREIGN KEY (`round_status`) REFERENCES `round_status` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `round`
--

LOCK TABLES `round` WRITE;
/*!40000 ALTER TABLE `round` DISABLE KEYS */;
INSERT INTO `round` VALUES (1,1,1,'1',NULL),(2,1,1,'2',NULL),(3,1,1,'3',NULL),(4,1,1,'4',NULL),(5,1,1,'5',NULL);
/*!40000 ALTER TABLE `round` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `round_status`
--

DROP TABLE IF EXISTS `round_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `round_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `round_status`
--

LOCK TABLES `round_status` WRITE;
/*!40000 ALTER TABLE `round_status` DISABLE KEYS */;
INSERT INTO `round_status` VALUES (1,'created',''),(2,'prepared','');
/*!40000 ALTER TABLE `round_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sponsor`
--

DROP TABLE IF EXISTS `sponsor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sponsor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `banner` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sponsor`
--

LOCK TABLES `sponsor` WRITE;
/*!40000 ALTER TABLE `sponsor` DISABLE KEYS */;
/*!40000 ALTER TABLE `sponsor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `team`
--

DROP TABLE IF EXISTS `team`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `team` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `flag` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `team`
--

LOCK TABLES `team` WRITE;
/*!40000 ALTER TABLE `team` DISABLE KEYS */;
INSERT INTO `team` VALUES (1,'Boca','http://coolspotters.com/files/photos/31571/boca-juniors-profile.gif'),(2,'River','http://www.soccershopusa.com/Images_Products/DSC_1404.jpg'),(3,'Racing','http://tipsterarea.com/images/teams/argentina/racing-club.gif'),(4,'Independiente','http://icons.iconarchive.com/icons/giannis-zographos/south-american-football-club/256/Independiente-icon.png'),(5,'Estudiantes','http://icons.iconarchive.com/icons/giannis-zographos/south-american-football-club/256/Estudiantes-icon.png'),(6,'Gimnasia','http://png-1.vector.me/files/images/7/9/793500/gimnasia_y_esgrima_la_plata_thumb.png');
/*!40000 ALTER TABLE `team` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `team_in_tournament`
--

DROP TABLE IF EXISTS `team_in_tournament`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `team_in_tournament` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `team_id` int(11) DEFAULT NULL,
  `tournament_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_8780F5FE296CD8AE` (`team_id`),
  KEY `IDX_8780F5FE33D1A3E7` (`tournament_id`),
  CONSTRAINT `FK_8780F5FE296CD8AE` FOREIGN KEY (`team_id`) REFERENCES `team` (`id`),
  CONSTRAINT `FK_8780F5FE33D1A3E7` FOREIGN KEY (`tournament_id`) REFERENCES `tournament` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `team_in_tournament`
--

LOCK TABLES `team_in_tournament` WRITE;
/*!40000 ALTER TABLE `team_in_tournament` DISABLE KEYS */;
INSERT INTO `team_in_tournament` VALUES (1,3,NULL),(2,6,NULL),(3,5,NULL),(4,1,NULL),(5,4,NULL);
/*!40000 ALTER TABLE `team_in_tournament` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `team_match_participation`
--

DROP TABLE IF EXISTS `team_match_participation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `team_match_participation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `team_in_tournament` int(11) DEFAULT NULL,
  `football_match` int(11) DEFAULT NULL,
  `played_football_match` int(11) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `payment` double DEFAULT NULL,
  `goals_for` int(11) DEFAULT NULL,
  `goals_against` int(11) DEFAULT NULL,
  `winner` tinyint(1) DEFAULT NULL,
  `points` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_CD04CF778CE33ACE` (`football_match`),
  UNIQUE KEY `UNIQ_CD04CF77F58CFA5D` (`played_football_match`),
  KEY `IDX_CD04CF778780F5FE` (`team_in_tournament`),
  CONSTRAINT `FK_CD04CF778780F5FE` FOREIGN KEY (`team_in_tournament`) REFERENCES `team_in_tournament` (`id`),
  CONSTRAINT `FK_CD04CF778CE33ACE` FOREIGN KEY (`football_match`) REFERENCES `football_match` (`id`),
  CONSTRAINT `FK_CD04CF77F58CFA5D` FOREIGN KEY (`played_football_match`) REFERENCES `football_match` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `team_match_participation`
--

LOCK TABLES `team_match_participation` WRITE;
/*!40000 ALTER TABLE `team_match_participation` DISABLE KEYS */;
INSERT INTO `team_match_participation` VALUES (1,5,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL),(2,1,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL);
/*!40000 ALTER TABLE `team_match_participation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teammatchparticipation_player`
--

DROP TABLE IF EXISTS `teammatchparticipation_player`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teammatchparticipation_player` (
  `teammatchparticipation_id` int(11) NOT NULL,
  `player_id` int(11) NOT NULL,
  PRIMARY KEY (`teammatchparticipation_id`,`player_id`),
  KEY `IDX_73A31C5053F82A41` (`teammatchparticipation_id`),
  KEY `IDX_73A31C5099E6F5DF` (`player_id`),
  CONSTRAINT `FK_73A31C5053F82A41` FOREIGN KEY (`teammatchparticipation_id`) REFERENCES `team_match_participation` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_73A31C5099E6F5DF` FOREIGN KEY (`player_id`) REFERENCES `player` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teammatchparticipation_player`
--

LOCK TABLES `teammatchparticipation_player` WRITE;
/*!40000 ALTER TABLE `teammatchparticipation_player` DISABLE KEYS */;
INSERT INTO `teammatchparticipation_player` VALUES (2,1);
/*!40000 ALTER TABLE `teammatchparticipation_player` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tournament`
--

DROP TABLE IF EXISTS `tournament`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tournament` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_id` int(11) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `isRegistrationOpen` tinyint(1) DEFAULT NULL,
  `dateBegin` datetime NOT NULL,
  `dateEnd` datetime NOT NULL,
  `inscriptionCostPerTeam` double NOT NULL,
  `matchCostPerTeam` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_BD5FB8D9C54C8C93` (`type_id`),
  KEY `IDX_BD5FB8D96BF700BD` (`status_id`),
  CONSTRAINT `FK_BD5FB8D96BF700BD` FOREIGN KEY (`status_id`) REFERENCES `tournament_status` (`id`),
  CONSTRAINT `FK_BD5FB8D9C54C8C93` FOREIGN KEY (`type_id`) REFERENCES `tournament_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tournament`
--

LOCK TABLES `tournament` WRITE;
/*!40000 ALTER TABLE `tournament` DISABLE KEYS */;
INSERT INTO `tournament` VALUES (1,1,1,'Prueba',0,'2010-01-01 00:00:00','2010-01-01 00:00:00',500,50);
/*!40000 ALTER TABLE `tournament` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tournament_sponsor`
--

DROP TABLE IF EXISTS `tournament_sponsor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tournament_sponsor` (
  `tournament_id` int(11) NOT NULL,
  `sponsor_id` int(11) NOT NULL,
  PRIMARY KEY (`tournament_id`,`sponsor_id`),
  KEY `IDX_EA8332DE33D1A3E7` (`tournament_id`),
  KEY `IDX_EA8332DE12F7FB51` (`sponsor_id`),
  CONSTRAINT `FK_EA8332DE12F7FB51` FOREIGN KEY (`sponsor_id`) REFERENCES `sponsor` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_EA8332DE33D1A3E7` FOREIGN KEY (`tournament_id`) REFERENCES `tournament` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tournament_sponsor`
--

LOCK TABLES `tournament_sponsor` WRITE;
/*!40000 ALTER TABLE `tournament_sponsor` DISABLE KEYS */;
/*!40000 ALTER TABLE `tournament_sponsor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tournament_status`
--

DROP TABLE IF EXISTS `tournament_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tournament_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tournament_status`
--

LOCK TABLES `tournament_status` WRITE;
/*!40000 ALTER TABLE `tournament_status` DISABLE KEYS */;
INSERT INTO `tournament_status` VALUES (1,'abierto',''),(2,'finalizado',''),(3,'suspendido','');
/*!40000 ALTER TABLE `tournament_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tournament_team`
--

DROP TABLE IF EXISTS `tournament_team`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tournament_team` (
  `tournament_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  PRIMARY KEY (`tournament_id`,`team_id`),
  KEY `IDX_F36D142133D1A3E7` (`tournament_id`),
  KEY `IDX_F36D1421296CD8AE` (`team_id`),
  CONSTRAINT `FK_F36D1421296CD8AE` FOREIGN KEY (`team_id`) REFERENCES `team` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_F36D142133D1A3E7` FOREIGN KEY (`tournament_id`) REFERENCES `tournament` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tournament_team`
--

LOCK TABLES `tournament_team` WRITE;
/*!40000 ALTER TABLE `tournament_team` DISABLE KEYS */;
INSERT INTO `tournament_team` VALUES (1,1),(1,3),(1,4),(1,5),(1,6);
/*!40000 ALTER TABLE `tournament_team` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tournament_type`
--

DROP TABLE IF EXISTS `tournament_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tournament_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tournament_type`
--

LOCK TABLES `tournament_type` WRITE;
/*!40000 ALTER TABLE `tournament_type` DISABLE KEYS */;
INSERT INTO `tournament_type` VALUES (1,'Tradicional',''),(2,'Llaves',''),(3,'Grupos y llaves',''),(4,'Amistoso','');
/*!40000 ALTER TABLE `tournament_type` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-09-29 18:24:32
