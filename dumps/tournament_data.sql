-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 25-09-2015 a las 07:25:47
-- Versión del servidor: 5.5.44-0ubuntu0.14.04.1
-- Versión de PHP: 5.5.9-1ubuntu4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `tournament`
--

--
-- Volcado de datos para la tabla `football_match`
--

INSERT INTO `football_match` (`id`, `local_id`, `visitor_id`, `round_id`, `court_id`, `referee_id`, `status_id`, `local_participation`, `visitor_participation`, `played_football_match`, `orderInRound`, `dateAndTime`, `refereeObservations`, `visitor_goals`, `local_goals`) VALUES
(1, 2, 5, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, -1, -1),
(2, 3, 4, 1, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, -1, -1),
(3, 5, 1, 2, NULL, NULL, 2, 1, 2, 1, 0, NULL, NULL, -1, -1),
(4, 3, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, -1, -1),
(5, 1, 4, 3, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, -1, -1),
(6, 5, 3, 3, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, -1, -1),
(7, 3, 1, 4, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, -1, -1),
(8, 2, 4, 4, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, -1, -1),
(9, 1, 2, 5, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, -1, -1),
(10, 4, 5, 5, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, -1, -1);

--
-- Volcado de datos para la tabla `football_match_status`
--

INSERT INTO `football_match_status` (`id`, `title`, `description`) VALUES
(1, 'preparado', ''),
(2, 'finalizado', ''),
(3, 'suspendido', ''),
(4, 'cancelado', ''),
(5, 'creado', '');

--
-- Volcado de datos para la tabla `fos_user_user`
--

INSERT INTO `fos_user_user` (`id`, `managed_team`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `locked`, `expired`, `expires_at`, `confirmation_token`, `password_requested_at`, `roles`, `credentials_expired`, `credentials_expire_at`, `created_at`, `updated_at`, `date_of_birth`, `firstname`, `lastname`, `website`, `biography`, `gender`, `locale`, `timezone`, `phone`, `facebook_uid`, `facebook_name`, `facebook_data`, `twitter_uid`, `twitter_name`, `twitter_data`, `gplus_uid`, `gplus_name`, `gplus_data`, `token`, `two_step_code`) VALUES
(1, NULL, 'superadmin', 'superadmin', 'contacto@superadmin.com.ar', 'contacto@superadmin.com.ar', 1, 'katxugvvnlwg8s48gg4kgwwg8cgwg0k', 'tODR3gJU5X4/P1oLrLi8s04/zb60Q8Pv3IHHQ5baYwU8i/9AhClj7R2283po/rpn738a5GUfu/1dJp/h24/VSg==', '2015-09-03 22:46:23', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:16:"ROLE_SUPER_ADMIN";}', 0, NULL, '2015-07-15 23:37:19', '2015-09-03 22:46:23', NULL, NULL, NULL, NULL, NULL, 'u', NULL, NULL, NULL, NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL),
(2, NULL, 'delegate', 'delegate', 'contacto@delegate.com', 'contacto@delegate.com', 1, '2wd3p0121u68k40s880080kgcsk0c4c', 'GITlzb95U4iZ/QgvsphI6xr1mUqljmj2SAKBombkVtyxA0jH6n5IFd4+UXcKF0W/ZSuvc4SabhzQJLOFTF7OVw==', '2015-09-09 20:29:51', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:13:"ROLE_DELEGATE";}', 0, NULL, '2015-09-06 20:06:58', '2015-09-09 20:29:51', NULL, NULL, NULL, NULL, NULL, 'u', NULL, NULL, NULL, NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL),
(3, NULL, 'admin', 'admin', 'contacto@admin.com', 'contacto@admin.com', 1, 'qm8450166qogc4k0o0440gocgg8w44k', '/eB4rm/6oeingTl8ZOTQXSu22RquPXfwonbaX4c9DKU7qXcIGDZRv+MojJk9tri34ivm7jYJ6E0IpibvoWop0A==', '2015-09-23 21:54:56', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:10:"ROLE_ADMIN";}', 0, NULL, '2015-09-06 20:07:41', '2015-09-23 21:54:56', NULL, NULL, NULL, NULL, NULL, 'u', NULL, NULL, NULL, NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL);

--
-- Volcado de datos para la tabla `played_football_match`
--

INSERT INTO `played_football_match` (`id`, `football_match`, `local_participation`, `visitor_participation`, `dateAndTime`, `court`, `referee`, `refereeObservations`) VALUES
(1, NULL, 1, 2, NULL, NULL, NULL, NULL);

--
-- Volcado de datos para la tabla `player`
--

INSERT INTO `player` (`id`, `team_id`, `lastname`, `firstname`, `dni`, `birth`, `email`, `healthInfo`, `number`) VALUES
(1, 1, 'Villareal', 'Gonzalo', '28671089', '1925-01-01 00:00:00', 'gonetil@gmail.com', 'en forma', 11),
(2, NULL, 'Lira', 'Ariel', '29992332', '1983-04-01 00:00:00', 'arieljlira@gmail.com', NULL, 10),
(3, NULL, 'Gianotti', 'Jorge Oscar', '33218971', '1987-08-05 00:00:00', 'jorge.oscar.gianotti@gmail.com', NULL, 14);

--
-- Volcado de datos para la tabla `player_participation`
--

INSERT INTO `player_participation` (`id`, `player`, `team_match_participation`, `yellow_cards`, `red_cards`, `goals`, `suspended_dates`, `tshirt_number`) VALUES
(1, 1, 2, 1, 0, 0, 0, NULL);

--
-- Volcado de datos para la tabla `round`
--

INSERT INTO `round` (`id`, `tournament`, `round_status`, `number`, `estimatedDate`) VALUES
(1, 1, 1, '1', NULL),
(2, 1, 1, '2', NULL),
(3, 1, 1, '3', NULL),
(4, 1, 1, '4', NULL),
(5, 1, 1, '5', NULL);

--
-- Volcado de datos para la tabla `round_status`
--

INSERT INTO `round_status` (`id`, `title`, `description`) VALUES
(1, 'created', ''),
(2, 'prepared', '');

--
-- Volcado de datos para la tabla `team`
--

INSERT INTO `team` (`id`, `name`, `flag`) VALUES
(1, 'Boca', 'http://coolspotters.com/files/photos/31571/boca-juniors-profile.gif'),
(2, 'River', 'http://www.soccershopusa.com/Images_Products/DSC_1404.jpg'),
(3, 'Racing', 'http://tipsterarea.com/images/teams/argentina/racing-club.gif'),
(4, 'Independiente', 'http://icons.iconarchive.com/icons/giannis-zographos/south-american-football-club/256/Independiente-icon.png'),
(5, 'Estudiantes', 'http://icons.iconarchive.com/icons/giannis-zographos/south-american-football-club/256/Estudiantes-icon.png'),
(6, 'Gimnasia', 'http://png-1.vector.me/files/images/7/9/793500/gimnasia_y_esgrima_la_plata_thumb.png');

--
-- Volcado de datos para la tabla `teammatchparticipation_player`
--

INSERT INTO `teammatchparticipation_player` (`teammatchparticipation_id`, `player_id`) VALUES
(2, 1);

--
-- Volcado de datos para la tabla `team_in_tournament`
--

INSERT INTO `team_in_tournament` (`id`, `team_id`, `tournament_id`) VALUES
(1, 3, NULL),
(2, 6, NULL),
(3, 5, NULL),
(4, 1, NULL),
(5, 4, NULL);

--
-- Volcado de datos para la tabla `team_match_participation`
--

INSERT INTO `team_match_participation` (`id`, `team_in_tournament`, `football_match`, `played_football_match`, `date`, `payment`, `goals_for`, `goals_against`, `winner`, `points`) VALUES
(1, 5, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL),
(2, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL);

--
-- Volcado de datos para la tabla `tournament`
--

INSERT INTO `tournament` (`id`, `type_id`, `status_id`, `name`, `isRegistrationOpen`, `dateBegin`, `dateEnd`, `inscriptionCostPerTeam`, `matchCostPerTeam`) VALUES
(1, 1, 1, 'Prueba', 0, '2010-01-01 00:00:00', '2010-01-01 00:00:00', 500, 50);

--
-- Volcado de datos para la tabla `tournament_status`
--

INSERT INTO `tournament_status` (`id`, `title`, `description`) VALUES
(1, 'abierto', ''),
(2, 'finalizado', ''),
(3, 'suspendido', '');

--
-- Volcado de datos para la tabla `tournament_team`
--

INSERT INTO `tournament_team` (`tournament_id`, `team_id`) VALUES
(1, 1),
(1, 3),
(1, 4),
(1, 5),
(1, 6);

--
-- Volcado de datos para la tabla `tournament_type`
--

INSERT INTO `tournament_type` (`id`, `title`, `description`) VALUES
(1, 'Tradicional', ''),
(2, 'Llaves', ''),
(3, 'Grupos y llaves', ''),
(4, 'Amistoso', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
