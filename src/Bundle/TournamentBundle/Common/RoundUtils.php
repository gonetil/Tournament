<?php

namespace Bundle\TournamentBundle\Common;

use Bundle\TournamentBundle\Entity\Round;

class RoundUtils
{

    const CREATED_ROUND = 'created';
    const PREPARED_ROUND = 'prepared';
    const FINISHED_ROUND = 'finished';
    const SUSPENDED_ROUND = 'suspended';
    const CANCELLED_ROUND = 'cancelled';

    public static function isRoundPrepared(Round $round)
    {
        return (($status = $round->getStatus()) && ( ( $status->getTitle() === self::PREPARED_ROUND)));
    }

}
