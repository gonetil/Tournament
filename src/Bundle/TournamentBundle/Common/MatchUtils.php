<?php
/**
 * Created by PhpStorm.
 * User: gonetil
 * Date: 30/04/15
 * Time: 18:20
 */

namespace Bundle\TournamentBundle\Common;

class MatchUtils {

    const CREATED_MATCH = 0;
    const PREPARED_MATCH = 1;
    const FINISHED_MATCH = 2;
    const SUSPENDED_MATCH = 3;
    const CANCELLED_MATCH = 4;


    public static function isMatchEnded($match)
    {
        return (($status = $match->getStatus())
                && ( ( $status->getId() == SELF::FINISHED_MATCH) || ($status->getId() == SELF::SUSPENDED_MATCH)));
        }

    public static function isLocalWinner($match) {
        return ($match->getLocalGoals() > $match->getVisitorGoals());
    }
    public static function isVisitorWinner($match) {
        return ($match->getLocalGoals() < $match->getVisitorGoals());
    }

    public static function visitorPoints($match)
    {
        $points = 1;  //tie
        if (MatchUtils::isVisitorWinner($match))
            $points = 3;
        elseif (MatchUtils::isLocalWinner($match))
            $points= 0;
        return $points;
    }

    public static function localPoints($match)
    {
        $points = 1;  //tie
        if (MatchUtils::isVisitorWinner($match))
            $points = 0;
        elseif (MatchUtils::isLocalWinner($match))
            $points= 3;
        return $points;
    }



}