<?php

namespace Bundle\TournamentBundle\Form;

use Bundle\TournamentBundle\EventListener\AddPlayerFieldSubscriber;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Bundle\TournamentBundle\Form\DataTransformer\ArrayToTeamMatchParticipationTransformer;

class TeamMatchParticipationType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $teamInTournament = $options['data']['teamInTournament'];
        
        $builder->addEventSubscriber(new AddPlayerFieldSubscriber('players'));

        $builder->add('teamInTournament', 'entity', array('class' => 'Bundle\TournamentBundle\Entity\TeamInTournament', 'label' => 'Equipo', 'read_only' => true ,'attr' => array('class' => 'team_select'),
            'query_builder' =>function(EntityRepository $er) use ($teamInTournament){
                        return $er->createQueryBuilder('t')->where('t.id = :tid')
                                ->setParameter('tid', $teamInTournament->getId());
                    }
                    ))
                ->add('players', 'entity', array('class' => 'Bundle\TournamentBundle\Entity\Player',
                    'multiple' => true,
                    'attr' => array('class' => 'players_select'),
                    'required' => false,
                    'query_builder' => function(EntityRepository $er) use($teamInTournament){
                        return $er->createQueryBuilder('p')->where('p.team = :tit')
                                ->setParameter('tit', $teamInTournament);
                    })
                )
                ->add('payment', 'integer', array('label' => 'Monto abonado', 'required' => false, 'data' => 0))
                ->add('points', 'integer', array('label' => 'Puntos', 'required' => false, 'data' => 0))
                ->add('goals_for', 'integer', array('label' => 'Goles a favor', 'required' => false, 'data' => 0))
                ->add('goals_against', 'integer', array('label' => 'Goles en contra', 'required' => false, 'data' => 0))
                ->add('is_winner', 'checkbox', array('label' => 'Equipo ganador?', 'required' => false))
        ;
//                    $transformer = new ArrayToTeamMatchParticipationTransformer();
//                    $builder->addModelTransformer($transformer);
    }

    public function getName()
    {
        return 'bundle_tournamentbundle_teammatchparticipationtype';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null,//'Bundle\TournamentBundle\Entity\TeamMatchParticipation',
            'csrf_protection' => false,
        ));
    }

}
