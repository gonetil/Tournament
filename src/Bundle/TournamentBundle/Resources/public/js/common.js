$(document).ready(function () {
    var get_players = function () {
        console.log($(this).val());
        var team_in_tournament_id = $(this).val();
        $.ajax({
            type: 'get',
            url: Routing.generate('tournament_teamintournament_players', {'id': team_in_tournament_id}),
            success: function (data) {
                var $players_selector = $('select.players_select');

                $players_selector.html('');
                data = JSON.parse(data);
                console.log(data);
                for (var i = 0, total = data.length; i < total; i++) {
                    $players_selector.append('<option value="' + data[i].id + '">' + data[i].lastname + ', ' + data[i].firstname + '</option>');
                }
            }
        });
    }
//    $("select.team_select").load(get_players);
//    get_players();
    $("select.team_select").change(get_players);
});