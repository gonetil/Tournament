<?php

namespace Bundle\TournamentBundle\Block;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Sonata\BlockBundle\Model\BlockInterface;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\CoreBundle\Validator\ErrorElement;
use Sonata\BlockBundle\Block\BaseBlockService;
use Sonata\BlockBundle\Block\BlockServiceInterface;
use Sonata\BlockBundle\Block\BlockAdminServiceInterface;

class FechaActualBlockService extends BaseBlockService implements BlockServiceInterface
{

    private $container;

    public function __construct($name, $templating, $container)
    {
        parent::__construct($name, $templating);

        $this->container = $container;
    }

    public function buildCreateForm(FormMapper $form, BlockInterface $block)
    {
        
    }

    public function buildEditForm(FormMapper $form, BlockInterface $block)
    {
        $formMapper->add('settings', 'sonata_type_immutable_array', array(
            'keys' => array(
                array('url', 'url', array('required' => false)),
                array('title', 'text', array('required' => false)),
            )
        ));
    }

    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
        $em = $this->container->get('doctrine.orm.entity_manager');
        $tm = $this->container->get('tournament.manager.tournament_manager');

        $tournaments = $em->getRepository('TournamentBundle:Tournament')->getActiveTournaments();

        $actualRounds = array();
        $nextRounds = array();
        foreach ($tournaments as $tournament) {
            $actualRounds[$tournament->getId()] = $tm->getActualRound($tournament);
            $nextRounds[$tournament->getId()] = $tm->getNextRound($tournament);
        }

        return $this->renderResponse($blockContext->getTemplate(), array(
                    'tournaments' => $tournaments,
                    'actualRounds' => $actualRounds,
                    'nextRounds' => $nextRounds,
                    'settings' => $blockContext->getSettings(),
                    'block' => $blockContext->getBlock()
                        ), $response);
    }

    public function getCacheKeys(BlockInterface $block)
    {
        return array();
    }

    public function getJavascripts($media)
    {
        return array();
    }

    public function getName()
    {
        return 'nombre';
    }

    public function getStylesheets($media)
    {
        return array();
    }

    public function load(BlockInterface $block)
    {
        
    }

    public function setDefaultSettings(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'url' => false,
            'title' => 'Fecha actual',
            'template' => 'TournamentBundle:Block:block_fecha_actual.html.twig',
        ));
    }

    public function validateBlock(ErrorElement $errorElement, BlockInterface $block)
    {
//        $errorElement
//                ->with('settings.url')
//                ->assertNotNull(array())
//                ->assertNotBlank()
//                ->end()
//                ->with('settings.title')
//                ->assertNotNull(array())
//                ->assertNotBlank()
//                ->assertMaxLength(array('limit' => 50))
//                ->end();
    }

    public function getBlockMetadata($code = null)
    {
        
    }

}
