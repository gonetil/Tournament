<?php

namespace Bundle\TournamentBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Bundle\TournamentBundle\Entity\TournamentStatus;
use Doctrine\ORM\QueryBuilder;
use Celsius3\CoreBundle\Entity\Instance;
use Celsius3\CoreBundle\Manager\UserManager;

class TournamentRepository extends EntityRepository
{

    public function getActiveTournaments()
    {
        $qb = $this->createQueryBuilder('t')
                ->select('t,r,m')
                ->innerJoin('t.status', 's')
                ->innerJoin('t.rounds', 'r')
                ->innerJoin('r.matches', 'm')
                ->where('s.title = :title')
                ->setParameter('title', TournamentStatus::OPEN);

        return $qb->getQuery()->execute();
    }

}
