<?php

namespace Bundle\TournamentBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class RoundAdmin extends Admin
{

    protected $baseRouteName = "tournament_round";
    protected $baseRoutePattern = 'round';

//    protected $parentAssociationMapping = 'tournament';

    private function getFields($mapper, $full = false)
    {
        $mapper
                ->add('number', null, array('label' => 'Número', 'read_only' => true))
                ->add('estimatedDate', null, array('label' => 'Fecha estimada'))
                ->add('tournament', null, array('label' => 'Torneo', 'read_only' => true, 'disabled' => true))
        ;
        if ($full) {
            $mapper->add('matches');
        }
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $this->getFields($datagridMapper);
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $this->getFields($listMapper);
        $listMapper
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    )
                ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $this->getFields($formMapper);
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $this->getFields($showMapper, true);
    }

    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);

        $id = $this->getRequest()->get('id');

        if ($id) {
            $query->andWhere(
                    $query->expr()->eq($query->getRootAliases()[0] . '.tournament', ':tournament')
            );
            $query->setParameter('tournament', $id);
        }

        return $query;
    }

}
