<?php

namespace Bundle\TournamentBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection as RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;

class TeamAdmin extends Admin
{

    protected $baseRouteName = "tournament_team";
    protected $baseRoutePattern = 'team';

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('name')
                ->add('flag')
                ->add('delegates')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->add('name', null, array('label' => 'Nombre', 'template' => 'TournamentBundle:Team:team_tiny_flag.html.twig'))
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    )
                ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
                ->add('name', null, array('label' => 'Nombre'))
                ->add('flag', null, array('label' => 'URL de la bandera'))
                ->add('delegates', null, array('label' => 'Delegados'))
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
                ->add('name')
                ->add('flag')
        ;
    }

    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        $user = $this->getConfigurationPool()->getContainer()->get('security.context')->getToken()->getUser();

        if ($user->hasRole('ROLE_DELEGATE')) {
            $query->andWhere($query->getRootAliases()[0] . ' = :team')
                    ->setParameter('team', $user->getManagedTeam());
        }

        return $query;
    }

    public function prePersist($team)
    {
        $this->preUpdate($team);
    }

    public function preUpdate($team)
    {
        $team->setDelegates($team->getDelegates());
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        parent::configureRoutes($collection);
        $collection
                ->add('currentAccount', $this->getRouterIdParameter() . '/current_account')
        ;
    }

}
