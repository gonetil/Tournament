<?php

namespace Bundle\TournamentBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Bundle\TournamentBundle\EventListener\AddPlayerFieldSubscriber;
use Bundle\TournamentBundle\Common\MatchUtils;

class TeamMatchParticipationAdmin extends Admin
{

    protected $baseRouteName = "tournament_teammatchparticipation";
    protected $baseRoutePattern = 'team_match_participation';

//    protected $parentAssociationMapping = 'footballmatch';

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('date', null, array('label' => 'Día'))
                ->add('payment', null, array('label' => 'Pago'))
                ->add('is_winner', null, array('label' => 'Ganador'))
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->add('date')
                ->add('payment')
                ->add('goals_for')
                ->add('goals_against')
                ->add('is_winner', null, array('label' => 'Ganador'))
                ->add('points', null, array('label' => 'Puntos'))
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    )
                ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $teamMatchParticipation = $this->getSubject();

        $em = $this->getConfigurationPool()->getContainer()->get('doctrine.orm.entity_manager');
        $query = $em->createQueryBuilder()
                ->select('p')
                ->from('TournamentBundle:Player', 'p')
                ->andWhere('p.teamInTournament = :teamInTournament')
                ->setParameter('teamInTournament', $teamMatchParticipation->getTeamInTournament());

        $teamOptions = array(
            'label' => 'Equipo en el torneo',
            'attr' => array('class' => 'team_select')
        );

        if (!is_null($teamMatchParticipation->getTeamInTournament())) {
            $teamOptions['read_only'] = true;
            $teamOptions['disabled'] = true;
        }

        $formMapper
                ->add('teamInTournament', null, $teamOptions)
                ->add('payment', null, array('label' => 'Monto abonado'))
                ->add('goals_for', null, array('label' => 'Goles a favor'))
                ->add('goals_against', null, array('label' => 'Goles en contra'))
                ->add('is_winner', null, array('label' => 'Ganó?'))
                ->add('points', null, array('label' => 'Puntos'))
                ->add('players', null, array(
                    'label' => 'Jugadores',
                    'multiple' => true,
                    'attr' => array('class' => 'players_select'),
                    'query_builder' => $query
                ))
                ->add('playersParticipation', 'sonata_type_collection', array(
                    'btn_add' => false,
                    'required' => false,
                    'type_options' => array(
                        'delete' => false
                    )), array(
                    'edit' => 'inline',
                    'inline' => 'table'
                ))
                ->add('playerChanges', 'sonata_type_collection', array(
                    'btn_add' => 'true',
                    'by_reference' => false,
                    'type_options' => array(
                        'allow_add' => true,
                        'delete' => true
                    )), array(
//                    'edit' => 'inline',
                    'inline' => 'table'
                ))
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
                ->add('match')
                ->add('date', null, array('label' => 'Día'))
                ->add('payment', null, array('label' => 'Monto abonado'))
                ->add('goals_for', null, array('label' => 'Goles a favor'))
                ->add('goals_against', null, array('label' => 'Goles en contra'))
                ->add('is_winner', null, array('label' => 'Ganó?'))
                ->add('points', null, array('label' => 'Puntos'))
                ->add('players', null, array('label' => 'Jugadores'))
        ;
    }

}
