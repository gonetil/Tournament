<?php

namespace Bundle\TournamentBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Bundle\TournamentBundle\Common\MatchUtils;
use Bundle\TournamentBundle\Entity\PlayedFootballMatch;
use Bundle\TournamentBundle\Entity\TeamMatchParticipation;

class FootballMatchAdmin extends Admin
{

    private $footballMatchManager;
    private $currentAccountManager;
    protected $baseRouteName = "tournament_footballmatch";
    protected $baseRoutePattern = 'football_match';

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('dateAndTime', null, array('label' => 'Día/hora'))
                ->add('status', null, array('label' => 'Estado'))
                ->add('local', null, array('label' => 'Local'))
                ->add('visitor', null, array('label' => 'Visitante'))
                ->add('court', null, array('label' => 'Cancha'))

        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->add('local', null, array('label' => 'Local'))
                ->add('visitor', null, array('label' => 'Visitante'))
                ->add('round', null, array('label' => 'Fecha'))
                ->add('orderInRound', null, array('label' => 'Nro. de orden'))
                ->add('dateAndTime', null, array('label' => 'Día y hora', 'format' => 'd/m/Y H:i'))
                ->add('status', null, array('label' => 'Estado'))
                ->add('court', null, array('label' => 'Cancha'))
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    )
                ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $footballMatch = $this->getSubject();

        $localParticipation = $footballMatch->getLocalParticipation();
        if (!$localParticipation) {
            $localParticipation = new TeamMatchParticipation();
        }
        if ($footballMatch->getLocal()) {
            $localParticipation->setTeamInTournament($footballMatch->getLocal());
        }

        $visitorParticipation = $footballMatch->getVisitorParticipation();
        if (!$visitorParticipation) {
            $visitorParticipation = new TeamMatchParticipation();
        }
        if ($footballMatch->getVisitor()) {
            $visitorParticipation->setTeamInTournament($footballMatch->getVisitor());
        }

        $roundOptions = array('label' => 'Fecha');
        if ($footballMatch->getRound()) {
            $roundOptions['read_only'] = true;
            $roundOptions['disabled'] = true;
        }

        $formMapper
                ->with('Información general')
                ->add('round', null, $roundOptions)
                ->add('orderInRound', null, array('label' => 'Número de orden'))
                ->add('dateAndTime', 'sonata_type_datetime_picker', array(
                    'label' => 'Día y hora',
                    'format' => 'dd MMM yyyy - hh:mm',
                    'dp_use_current' => true,
                    'read_only' => true
                ))
                ->add('status', null, array('label' => 'Estado'))
                ->add('court', null, array('label' => 'Cancha'))
                ->add('referee', null, array('label' => 'Referee'))
                ->add('refereeObservations', null, array('label' => 'Observaciones del referee'))
                ->end()
                ->with("Equipo local", array('collapsed' => true))
                ->add('local_participation', 'sonata_type_admin', array('label' => false, 'btn_add' => false, 'delete' => false, 'data' => $localParticipation))
                ->end()
                ->with('Equipo visitante', array('collapsed' => true))
                ->add('visitor_participation', 'sonata_type_admin', array('label' => false, 'btn_add' => false, 'delete' => false, 'data' => $visitorParticipation))
                ->end()
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
                ->add('orderInRound')
                ->add('dateAndTime')
                ->add('status')
                ->add('local')
                ->add('visitor')
                ->add('referee')
                ->add('refereeObservations')
                ->add('court')

        ;
    }

    public function prePersist($match)
    {
        $this->preUpdate($match);
    }

    public function preUpdate($match)
    {
        
    }

    public function postPersist($match)
    {
        $this->postUpdate($match);
    }

    public function postUpdate($match)
    {
        $localParticipation = $match->getLocalParticipation();
        $this->footballMatchManager->updatePlayersParticipation($localParticipation);

        $visitorParticipation = $match->getVisitorParticipation();
        $this->footballMatchManager->updatePlayersParticipation($visitorParticipation);

        if (MatchUtils::isMatchEnded($match)) {
            $this->footballMatchManager->updatePlayedFootballMatch($match);

            $this->currentAccountManager->discountPlayedFootballMatch($match->getLocal(), $match);
            $this->currentAccountManager->discountPlayedFootballMatch($match->getVisitor(), $match);
        }
    }

    public function getFootballMatchManager()
    {
        return $this->footballMatchManager;
    }

    public function setFootballMatchManager($footballMatchManager)
    {
        $this->footballMatchManager = $footballMatchManager;
        return $this;
    }

    public function getCurrentAccountManager()
    {
        return $this->currentAccountManager;
    }

    public function setCurrentAccountManager($currentAccountManager)
    {
        $this->currentAccountManager = $currentAccountManager;
        return $this;
    }

}
