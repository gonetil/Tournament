<?php

namespace Bundle\TournamentBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection as RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;

class PlayerParticipationAdmin extends Admin
{

    protected $baseRouteName = "tournament_player_participation";
    protected $baseRoutePattern = 'player_participation';

//    protected $parentAssociationMapping = 'teammatchparticipation';

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
//        $datagridMapper
//                ->add('lastname')
//                ->add('dni')
//                ->add('birth', null, array('label' => 'Fecha de Nacimiento',
//                    'years' => range(1980, date('Y')),
//                    'format' => 'd-M-Y'))
//                ->add('email')
//        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->add('player', null, array('label' => 'Jugador'))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
                ->add('player', null, array('label' => 'Jugador'))
                ->add('yellowCards', null, array('label' => 'Tarjetas amarillas'))
                ->add('redCards', null, array('label' => 'Tarjetas rojas'))
                ->add('goals', null, array('label' => 'Goles'))
                ->add('suspendedDates', null, array('label' => 'Fechas de suspensión'))
                ->add('tshirtNumber', null, array('label' => 'Número de camiseta'))
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
                ->add('player', null, array('label' => 'Jugador'))
        ;
    }

}
