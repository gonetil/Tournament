<?php

namespace Bundle\TournamentBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class PaymentAdmin extends Admin
{

    protected $baseRouteName = "tournament_payment";
    protected $baseRoutePattern = 'payment';

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
        ->add('teamInTournament')
        ->add('reason')
        ->add('date', 'doctrine_orm_datetime_range', array('field_type' => 'sonata_type_datetime_range_picker', 'label' => 'Fecha'))
//        ->add('date', 'doctrine_orm_datetime', array('field_type' => 'sonata_type_datetime_picker'))
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->add('teamInTournament')
                ->add('date')
                ->add('reason')
                ->add('amount')
                ->add('description')
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    )
                ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
                ->add('teamInTournament')
                ->add('date')
                ->add('reason')
                ->add('amount')
                ->add('description')
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
                ->add('teamInTournament')
                ->add('date')
                ->add('reason')
                ->add('amount')
                ->add('description')
        ;
    }

}
