<?php

namespace Bundle\TournamentBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection as RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;

class PlayerAdmin extends Admin
{

    protected $baseRouteName = "tournament_player";
    protected $baseRoutePattern = 'player';

//    protected $parentAssociationMapping = 'teamInTournament';

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('lastname')
                ->add('dni')
                ->add('birth', null, array('label' => 'Fecha de Nacimiento',
                    'years' => range(1980, date('Y')),
                    'format' => 'd-M-Y'))
                ->add('email')
                ->add('teamInTournament')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->add('firstname', null, array('label' => 'Nombre'))
                ->add('lastname', null, array('label' => 'Apellido'))
                ->add('dni', null, array('label' => 'DNI'))
                ->add('number', null, array('label' => 'Número'))
                ->add('birth', null, array('label' => 'Fecha de Nacimiento',
                    'years' => range(1980, date('Y')),
                    'format' => 'd/m/y'))
                ->add('email', null, array('label' => 'Email'))
                ->add('teamInTournament', null, array('label' => 'Equipo'))
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    )
                ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
                ->add('firstname', null, array('label' => 'Nombre'))
                ->add('lastname', null, array('label' => 'Apellido'))
                ->add('dni')
                ->add('birth', 'date', array('label' => 'Fecha de Nacimiento',
                    'years' => range(date('Y') - 90, date('Y')),
                    'format' => 'd MM y', 'required' => true))
                ->add('number', null, array('label' => 'Número', 'required' => false))
                ->add('email', null, array('label' => 'Email', 'required' => false))
                ->add('healthInfo', null, array('label' => 'Información médica', 'required' => false))
                ->add('teamInTournament', null, array('label' => 'Equipo', 'required' => false))
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
                ->add('firstname', null, array('label' => 'Nombres'))
                ->add('lastname', null, array('label' => 'Apellido'))
                ->add('number', null, array('label' => 'Número'))
                ->add('dni', null, array('label' => 'DNI'))
                ->add('birth', null, array('label' => 'Fecha de Nacimiento', 'format' => 'd/m/Y'))
                ->add('email', null, array('label' => 'email'))
                ->add('healthInfo', null, array('label' => 'Info. de salud'))
                ->add('teamInTournament', null, array('label' => 'Equipo'))
        ;
    }

}
