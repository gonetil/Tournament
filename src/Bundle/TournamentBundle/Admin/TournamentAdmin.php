<?php

namespace Bundle\TournamentBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection as RouteCollection;
use Knp\Menu\ItemInterface;
use Sonata\AdminBundle\Admin\AdminInterface;
use Bundle\TournamentBundle\EventListener\GenerateFixtureSubscriber;
use Symfony\Component\DependencyInjection\ContainerInterface;

class TournamentAdmin extends Admin
{

    private $container;
    protected $baseRouteName = "tournament_tournament";
    protected $baseRoutePattern = 'tournament';

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('type', null, array('label' => 'Tipo de torneo'))
                ->add('status', null, array('label' => 'Estado'))
                ->add('name', null, array('label' => 'Nombre del torneo'))
                ->add('isRegistrationOpen', null, array('label' => 'Registro de equipos abierto?'))
                ->add('dateBegin', null, array('label' => 'Fecha de inicio'))
                ->add('dateEnd', null, array('label' => 'Fecha de finalización'))
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {

        $listMapper
                ->add('name', null, array('label' => 'Nombre del torneo'))
                ->add('type', null, array('label' => 'Tipo de torneo'))
                ->add('status', null, array('label' => 'Estado'))
                ->add('dateBegin', null, array('label' => 'Fecha de inicio'))
                ->add('dateEnd', null, array('label' => 'Fecha de finalización'))
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
//                        'delete' => array(),
//                        'autogenerateFixture' => array(
//                            'template' => 'TournamentBundle:Tournament:list__action_autogenerateFixture.html.twig'
//                        )
                    )
                ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
                ->add('type', null, array('label' => 'Tipo de torneo'))
                ->add('status', null, array('label' => 'Estado'))
                ->add('name', null, array('label' => 'Nombre del torneo'))
                ->add('isRegistrationOpen', null, array('label' => 'Registro de equipos abierto?'))
                ->add('dateBegin', null, array('label' => 'Fecha de inicio'))
                ->add('dateEnd', null, array('label' => 'Fecha de finalización'))
                ->add('inscriptionCostPerTeam', null, array('label' => 'Costo de la inscripción por equipo'))
                ->add('matchCostPerTeam', null, array('label' => 'Costo de cada partido por equipo'))
                ->add('teams', null, array('label' => 'Equipos participantes'))
                ->add('generateFixture', 'checkbox', array('label' => 'Generar fixture', 'mapped' => false, 'required' => false))
        ;

        $formMapper->getFormBuilder()
                ->addEventSubscriber(new GenerateFixtureSubscriber($this->getContainer()))
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
                ->add('type', null, array('label' => 'Tipo de torneo'))
                ->add('status', null, array('label' => 'Estado'))
                ->add('name', null, array('label' => 'Nombre del torneo'))
                ->add('isRegistrationOpen', null, array('label' => 'Registro de equipos abierto?'))
                ->add('dateBegin', null, array('label' => 'Fecha de inicio', 'format' => 'd/m/Y'))
                ->add('dateEnd', null, array('label' => 'Fecha de finalización', 'format' => 'd/m/Y'))
                ->add('inscriptionCostPerTeam', null, array('label' => 'Costo de la inscripción por equipo'))
                ->add('matchCostPerTeam', null, array('label' => 'Costo de cada partido por equipo'))
                ->add('teams', null, array('label' => 'Equipos participantes'))
                ->add('rounds', null, array('label' => 'Fechas planificadas'))
        ;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        parent::configureRoutes($collection);
        $collection
                ->add('autogenerateFixture', $this->getRouterIdParameter() . '/autogenerateFixture')
                ->add('positionsTable', $this->getRouterIdParameter() . '/positions_table')
                ->add('inscriptionsDiscount', $this->getRouterIdParameter() . '/inscriptions_discount')
                ->add('topScorers', $this->getRouterIdParameter() . '/top_scorers')
        ;
    }

    protected function configureSideMenu(ItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        parent::configureSideMenu($menu, $action, $childAdmin);

        $id = $this->getRequest()->get('id');

//        $menu->addChild(
//                $this->trans('Rounds'), array('uri' => $this->getChild('sonata.admin.round')->generateUrl('list', array('id' => $id)))
//        );
    }

    public function configure()
    {
        parent::configure();

        $this->setTemplate('edit', 'TournamentBundle:Tournament:edit.html.twig');
        $this->setTemplate('new', 'TournamentBundle:Tournament:new.html.twig');
    }

    function getContainer()
    {
        return $this->container;
    }

    function setContainer(ContainerInterface $container)
    {
        $this->container = $container;
    }

}
