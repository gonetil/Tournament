<?php

namespace Bundle\TournamentBundle\Manager;

use Doctrine\ORM\EntityManager;
use Bundle\TournamentBundle\Entity\FootballMatch;
use Bundle\TournamentBundle\Entity\Team;
use Bundle\TournamentBundle\Entity\TeamInTournament;
use Bundle\TournamentBundle\Entity\Tournament;
use Bundle\TournamentBundle\Entity\CurrentAccount;
use Bundle\TournamentBundle\Entity\PlayedFootballMatchEntry;
use Bundle\TournamentBundle\Entity\TournamentInscriptionEntry;

class CurrentAccountManager
{

    private $em;

    function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getCurrentAccount(Team $team)
    {
        $currentAccount = $this->em->getRepository('TournamentBundle:CurrentAccount')
                ->findOneBy(array('team' => $team));

        if (!$currentAccount) {
            $currentAccount = new CurrentAccount();
            $currentAccount->setName($team->getName());
            $currentAccount->setTeam($team);
        }

        $this->em->persist($currentAccount);
        $this->em->flush($currentAccount);

        return $currentAccount;
    }

    public function discountPlayedFootballMatch(TeamInTournament $teamInTournament, FootballMatch $match)
    {
        $currentAccount = $this->getCurrentAccount($teamInTournament->getTeam());

        if (!is_null($currentAccount)) {
            $playedFootballMatchEntry = $this->em->getRepository('TournamentBundle:PlayedFootballMatchEntry')
                    ->findOneBy(array(
                'currentAccount' => $currentAccount,
                'playedFootballMatch' => $match->getPlayedFootballMatch()
            ));

            if (!$playedFootballMatchEntry) {
                $playedFootballMatchEntry = new PlayedFootballMatchEntry();
                $playedFootballMatchEntry
                        ->setAmount(-1 * abs($teamInTournament->getTournament()->getMatchCostPerTeam()))
                        ->setDate(new \DateTime())
//                        ->setReason()
                        ->setCurrentAccount($currentAccount)
                        ->setPlayedFootballMatch($match->getPlayedFootballMatch());
            }

            $this->em->persist($playedFootballMatchEntry);
            $this->em->flush();
        }
    }

    public function discountTournamentInscription(Team $team, Tournament $tournament)
    {
        $currentAccount = $this->getCurrentAccount($team);

        $tournamentInscriptionEntry = $this->em->getRepository('TournamentBundle:TournamentInscriptionEntry')
                ->findOneBy(array(
            'currentAccount' => $currentAccount,
            'tournament' => $tournament
        ));

        if (!$tournamentInscriptionEntry) {
            $tournamentInscriptionEntry = new TournamentInscriptionEntry();
            $tournamentInscriptionEntry
                    ->setAmount(-1 * abs($tournament->getInscriptionCostPerTeam()))
                    ->setCurrentAccount($currentAccount)
                    ->setDate(new \DateTime())
                    ->setTournament($tournament);
        }

        $this->em->persist($tournamentInscriptionEntry);
        $this->em->flush($tournamentInscriptionEntry);
    }

}
