<?php

namespace Bundle\TournamentBundle\Manager;

use Doctrine\ORM\EntityManager;
use Bundle\TournamentBundle\Common\MatchUtils;
use Bundle\TournamentBundle\Entity\Team;
use Bundle\TournamentBundle\Entity\Round;
use Bundle\TournamentBundle\Entity\FootballMatch;
use Bundle\TournamentBundle\Entity\Tournament;
use Bundle\TournamentBundle\Entity\TeamInTournament;
use Bundle\TournamentBundle\Common\RoundUtils;

class FixtureManager
{

    private $em;

    function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function autogenerateFixture(Tournament $tournament)
    {
        $this->deleteRounds($tournament);

        $rounds = $this->makeFixture($tournament);

        foreach ($rounds as $round) {
            $tournament->addRound($round);
        }
    }

    private function deleteRounds(Tournament $tournament)
    {
        foreach ($tournament->getRounds() as $round) {
            $this->em->remove($round);
        }

        $this->em->flush();
    }

    /**
     *  crea un fixture aleatorio a partir de la lista de equipos del torneo
     *  esto deberia depender del tipo de torneo (quizas sea conveniente moverlo a TournamentType
     */
    private function makeFixture(Tournament $tournament)
    {

        $teams = $tournament->getTeams()->toArray();

        shuffle($teams);
        if ((count($teams) % 2) === 1) {  //necesitamos un equipo extra, será para las fechas libres
            $free_team = new Team();
            $free_team->setName("libre");
            $teams[] = $free_team;
        }

        $libreTeamsParticipation = array();
        foreach ($teams as $team) {
            $teamParticipation = new TeamInTournament();
            $teamParticipation->setTeam($team);
            $teamParticipation->setTournament($tournament);
            if ($team->getName() !== "libre") {
                $tournament->addTeamParticipation($teamParticipation);
            } else {
                $libreTeamsParticipation[] = $teamParticipation;
            }
        }

        $teamsParticipation = array_merge($tournament->getTeamsParticipation()->toArray(), $libreTeamsParticipation);

        $createdStatus = $this->em->getRepository('TournamentBundle:RoundStatus')
                ->findOneBy(array('title' => RoundUtils::CREATED_ROUND));

        $rounds = array();
        for ($i = 1; $i < count($teamsParticipation); $i++) {
            $round = new Round();
            $round->setNumber("$i");
            $round->setTournament($tournament);
            $round->setStatus($createdStatus);
            $rounds[] = $round;

            for ($j = 0; $j < (count($teamsParticipation) / 2); $j++) {
                $pair_index = $this->getPair($teamsParticipation, $j);

                //asigno local y visitante, tratando de nivelar la localia
                $local_team = ($i % 2 == 1) ? $teamsParticipation[$j] : $teamsParticipation[$pair_index];
                $visitor_team = ($i % 2 == 0) ? $teamsParticipation[$j] : $teamsParticipation[$pair_index];

                if ($local_team->getTeam()->getName() !== "libre" && $visitor_team->getTeam()->getName() !== "libre") {
                    $round->addMatch($this->buildFootballMatch($local_team, $visitor_team, $round, $j));
                }
            }

            $this->reorderTeams($teamsParticipation);
        }

        return $rounds;
    }

    private function getPair($array, $index)
    {
        return (count($array) - 1 - $index);
    }

    private function reorderTeams(array &$array)
    {
        $last = $array[count($array) - 1];
        for ($i = count($array) - 1; $i > 0; $i--) {
            $array[$i] = $array[$i - 1];
        }
        $array[1] = $last;
    }

    private function buildFootballMatch($local, $visitor, $round, $order)
    {
        $status = $this->em->getRepository('TournamentBundle:FootballMatchStatus')
                ->find(MatchUtils::CREATED_MATCH);

        $match = new FootballMatch();
        $match->setLocal($local);
        $match->setVisitor($visitor);
        $match->setRound($round);
        $match->setOrderInRound($order);
        $match->setStatus($status);

        return $match;
    }

    private function buildParticipation($match, $teamParticipation)
    {
        $p = new TeamMatchParticipation();
        $p->setTeam($teamParticipation)
                ->setMatch($match);
    }

}
