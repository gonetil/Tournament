<?php

namespace Bundle\TournamentBundle\Manager;

use Bundle\TournamentBundle\Entity\Tournament;
use Doctrine\ORM\EntityManager;

class TournamentManager
{

    private $em;

    function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getActualRound(Tournament $tournament)
    {
        $rounds = $this->em->getRepository('TournamentBundle:Round')
                ->findBy(array('tournament' => $tournament));

        $actualDate = new \DateTime();
        $actualRoundDate = $tournament->getDateBegin();
        $actualRound = null;

        foreach ($rounds as $round) {
            $estimatedDate = $round->getEstimatedDate();

            if ($estimatedDate <= $actualDate && $estimatedDate > $actualRoundDate) {
                $actualRound = $round;
                $actualRoundDate = $round->getEstimatedDate();
            }
        }

        return $actualRound;
    }

    public function getNextRound(Tournament $tournament)
    {
        $rounds = $this->em->getRepository('TournamentBundle:Round')
                ->findBy(array('tournament' => $tournament));

        $actualDate = new \DateTime();
        $nextRoundDate = $tournament->getDateEnd();
        $nextRound = null;

        foreach ($rounds as $round) {
            $estimatedDate = $round->getEstimatedDate();

            if ($estimatedDate > $actualDate && $estimatedDate < $nextRoundDate) {
                $nextRound = $round;
                $nextRoundDate = $round->getEstimatedDate();
            }
        }

        return $nextRound;
    }

}
