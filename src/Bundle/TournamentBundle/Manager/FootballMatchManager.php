<?php

namespace Bundle\TournamentBundle\Manager;

use Doctrine\ORM\EntityManager;
use Bundle\TournamentBundle\Entity\FootballMatch;
use Bundle\TournamentBundle\Entity\TeamMatchParticipation;
use Bundle\TournamentBundle\Entity\PlayerParticipation;
use Bundle\TournamentBundle\Entity\PlayedFootballMatch;

class FootballMatchManager
{

    private $em;

    function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function updatePlayersParticipation(TeamMatchParticipation $teamMatchParticipation)
    {
        $players = $teamMatchParticipation->getPlayers();
        $participations = $teamMatchParticipation->getPlayersParticipation();

        foreach ($players as $player) {
            $participation = $this->em->getRepository('TournamentBundle:PlayerParticipation')
                    ->findOneBy(array('teamMatchParticipation' => $teamMatchParticipation, 'player' => $player));

            if (!$participation) {
                $participation = new PlayerParticipation();
                $participation->setPlayer($player);
                $participation->setTeamMatchParticipation($teamMatchParticipation);

                $this->em->persist($teamMatchParticipation);
                $this->em->persist($participation);
                $this->em->flush();
            }
        }

//        foreach ($participations as $participation) {
//            if (!$players->indexOf($participation->getPlayer())) {
//                $this->em->remove($participation);
//                $this->em->flush();
//            }
//        }
    }

    public function updatePlayedFootballMatch(FootballMatch $match)
    {
        $playedFootballMatch = $match->getPlayedFootballMatch();

        if (!$playedFootballMatch) {
            $playedFootballMatch = new PlayedFootballMatch();
        }

        $playedFootballMatch
                ->setDateAndTime($match->getDateAndTime())
                ->setCourt($match->getCourt())
                ->setReferee($match->getReferee())
                ->setRefereeObservations($match->getRefereeObservations());

        $localParticipation = $match->getLocalParticipation();
        $playedFootballMatch->setLocalParticipation($localParticipation);
        $localParticipation->setPlayedFootballMatch($playedFootballMatch);
        $localParticipation->setFootballMatch($match);

        $visitorParticipation = $match->getVisitorParticipation();
        $playedFootballMatch->setVisitorParticipation($visitorParticipation);
        $visitorParticipation->setPlayedFootballMatch($playedFootballMatch);
        $visitorParticipation->setFootballMatch($match);

        $playedFootballMatch->setFootballMatch($match);

        $match->setPlayedFootballMatch($playedFootballMatch);

        $this->em->persist($localParticipation);
        $this->em->persist($visitorParticipation);
        $this->em->persist($playedFootballMatch);
        $this->em->persist($match);
        $this->em->flush();
    }

}
