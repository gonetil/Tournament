<?php

/**
 * Created by PhpStorm.
 * User: gonetil
 * Date: 29/04/15
 * Time: 18:53
 */

namespace Bundle\TournamentBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Bundle\TournamentBundle\Entity\FootballMatch;
use Bundle\TournamentBundle\Entity\TeamMatchParticipation;
use Bundle\TournamentBundle\Entity\PlayerParticipation;
use Bundle\TournamentBundle\Common;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\PersistentCollection;

class FootballMatchListener
{

    private function setParticipationFields($participation, $match, $team, $goals_for, $goals_against, $points, $is_winner)
    {
        $participation->setDate($match->getDateAndTime())
                ->setTeamInTournament($team)
                ->setGoalsFor($goals_for)
                ->setGoalsAgainst($goals_against)
                ->setIsWinner($is_winner)
                ->setPayment(0)
                ->setPoints($points)

        ;
        return $participation;
    }

    private function setUpdateParticipationFields($participation, $match, $team, $goals_for, $goals_against, $points, $is_winner)
    {
        $participation->setDate($match->getDateAndTime())
                ->setTeamInTournament($team)
//                ->setFootballMatch($match)
                ->setGoalsFor($goals_for)
                ->setGoalsAgainst($goals_against)
                ->setIsWinner($is_winner)
                ->setPayment(0)
                ->setPoints($points)

        ;
        return $participation;
    }

    private function createParticipation($match, $team, $goals_for, $goals_against, $points, $is_winner)
    {
        return $this->setParticipationFields(new TeamMatchParticipation(), $match, $team, $goals_for, $goals_against, $points, $is_winner);
    }

    private function createLocalParticipation($match)
    {
        return $this->createParticipation($match, $match->getLocal(), $match->getLocalGoals(), $match->getVisitorGoals(), Common\MatchUtils::localPoints($match), Common\MatchUtils::isLocalWinner($match));
    }

    private function createVisitorParticipation($match)
    {
        return $this->createParticipation($match, $match->getVisitor(), $match->getVisitorGoals(), $match->getLocalGoals(), Common\MatchUtils::visitorPoints($match), Common\MatchUtils::isVisitorWinner($match));
    }

    private function updateParticipation($participation, $match, $team, $goals_for, $goals_against, $points, $is_winner)
    {
        return $this->setUpdateParticipationFields($participation, $match, $team, $goals_for, $goals_against, $points, $is_winner);
    }

    private function updateLocalParticipation($match)
    {

        return $this->updateParticipation($match->getLocalParticipation(), $match, $match->getLocal(), $match->getLocalGoals(), $match->getVisitorGoals(), Common\MatchUtils::localPoints($match), Common\MatchUtils::isLocalWinner($match));
    }

    private function updateVisitorParticipation($match)
    {
        return $this->updateParticipation($match->getVisitorParticipation(), $match, $match->getVisitor(), $match->getVisitorGoals(), $match->getLocalGoals(), Common\MatchUtils::visitorPoints($match), Common\MatchUtils::isVisitorWinner($match));
    }

    private function updatePlayersParticipation(EntityManager $entityManager, TeamMatchParticipation $teamMatchParticipation)
    {
        $players = $teamMatchParticipation->getPlayers();
        $participations = $teamMatchParticipation->getPlayersParticipation();

        foreach ($players as $player) {
            $participation = $entityManager->getRepository('TournamentBundle:PlayerParticipation')
                    ->findOneBy(array('teamMatchParticipation' => $teamMatchParticipation, 'player' => $player));

            if (!$participation) {
                $participation = new PlayerParticipation();
                $participation->setPlayer($player);
                $participation->setTeamMatchParticipation($teamMatchParticipation);

                $entityManager->persist($participation);
                $entityManager->flush();
            }
        }

        foreach ($participations as $participation) {
            if (!$players->indexOf($participation->getPlayer())) {
                $entityManager->remove($participation);
                $entityManager->flush();
            }
        }
    }

    public function preUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $entityManager = $args->getEntityManager();

        if ($entity instanceof TeamMatchParticipation) {
            $this->updatePlayersParticipation($entityManager, $entity);
        }

        if (($entity instanceof FootballMatch) && ( Common\MatchUtils::isMatchEnded($entity))) {
            $local = (!is_null($entity->getLocalParticipation()) ) ? $this->updateLocalParticipation($entity) : $this->createLocalParticipation($entity);
            $entity->setLocalParticipation($local);

            $visitor = (!is_null($entity->getVisitorParticipation()) ) ? $this->updateVisitorParticipation($entity) : $this->createVisitorParticipation($entity);
            $entity->setVisitorParticipation($visitor);

            $entityManager->persist($local);
            $entityManager->persist($visitor);
            $entityManager->update($entity);

            $entityManager->flush();
        }
    }

}
