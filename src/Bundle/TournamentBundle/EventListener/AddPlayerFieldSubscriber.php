<?php

/**
 * Created by PhpStorm.
 * User: gonzalo
 * Date: 22/05/15
 * Time: 19:20
 */

namespace Bundle\TournamentBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\VarDumper\VarDumper;

class AddPlayerFieldSubscriber implements EventSubscriberInterface
{

    private $propertyPathToPlayer;

    public function __construct($propertyPathToPlayer)
    {
        $this->propertyPathToPlayer = $propertyPathToPlayer;
    }

    public static function getSubscribedEvents()
    {
        return array(
            FormEvents::PRE_SET_DATA => 'preSetData',
            FormEvents::PRE_SUBMIT => 'preSubmit'
        );
    }

    private function addPlayerForm($form, $team_id)
    {
        $formOptions = array(
            'class' => 'Bundle\TournamentBundle\Entity\Player',
            'empty_value' => 'jugador',
            'label' => 'Jugador',
            'attr' => array(
                'class' => 'city_selector',
            ),
            'query_builder' => function (EntityRepository $repository) use ($team_id) {
        $qb = $repository->createQueryBuilder('player')
                ->innerJoin('player.team', 'team')
                ->where('team.id = :team')
                ->setParameter('team', $team_id)
        ;

        return $qb;
    }
        );

        $form->add($this->propertyPathToPlayer, 'entity', $formOptions);
    }

    public function preSetData(FormEvent $event)
    {
//        die('preSetData');
        $data = $event->getData();
        $form = $event->getForm();

        if (null === $data) {
            return;
        }

        $accessor = PropertyAccess::createPropertyAccessor();
        $value = $accessor->getValue($data["teamInTournament"], $this->propertyPathToPlayer);
        $player = ($value) ? $value->first() : null;

        $team_id = ($player) ? $player->getTeam()->getId() : null;

        $this->addPlayerForm($form, $team_id);
    }

    public function preSubmit(FormEvent $event)
    {
//        die('preSubmit');
        $data = $event->getData();
        $form = $event->getForm();                                                      
//        VarDumper::dump($form);die;
        $team_id = array_key_exists('teamInTournament', $data) ? $data['teamInTournament'] : null;

        $this->addPlayerForm($form, $team_id);
//        die('otro');
    }

}
