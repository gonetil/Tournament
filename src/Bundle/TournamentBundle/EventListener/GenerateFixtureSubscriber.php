<?php

namespace Bundle\TournamentBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormError;
use Symfony\Component\DependencyInjection\ContainerInterface;

class GenerateFixtureSubscriber implements EventSubscriberInterface
{

    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public static function getSubscribedEvents()
    {
        return array(
            FormEvents::SUBMIT => 'submit'
        );
    }

    public function submit(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();
        
        $generateFixture = $form->get('generateFixture')->getData();
        
        if ($generateFixture && ($data->getTeams()->count() < 2)) {
            $form->get('generateFixture')->addError(new FormError('Se deben registrar al menos 2 equipos para generar el fixture.'));
        }
        
        if ($generateFixture && $form->get('generateFixture')->isValid()) {
            $this->container->get('tournament.manager.fixture')->autogenerateFixture($data);
        }
    }

}
