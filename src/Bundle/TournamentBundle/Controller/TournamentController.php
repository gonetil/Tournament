<?php

/**
 * Created by PhpStorm.
 * User: gonetil
 * Date: 23/04/15
 * Time: 19:20
 */

namespace Bundle\TournamentBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Bundle\TournamentBundle\Common\MatchUtils;

class TournamentController extends Controller
{

    public function autogenerateFixtureAction($id)
    {
        $tournament = $this->admin->getObject($id);

        if (!$tournament) {
            throw new NotFoundHttpException(sprintf('No se pudo encontrar el Torneo con id : %s', $id));
        }

        if ($tournament->getTeams()->count() < 2) {
            $this->addFlash('sonata_flash_error', 'El torneo debe tener al menos 2 equipos registrados.');
        } else {
            $this->get('tournament.manager.fixture')->autogenerateFixture($tournament);
            $this->admin->update($tournament);
            $this->addFlash('sonata_flash_success', 'Fixture preparado satisfactoriamente');
        }

        return new RedirectResponse($this->admin->generateUrl('show', array('id' => $tournament->getId())));
    }

    /**
     * @Template()
     */
    public function positionsTableAction($id)
    {
        $admin_pool = $this->get('sonata.admin.pool');

        $tournament = $this->admin->getObject($id);
        if (!$tournament) {
            throw new NotFoundHttpException(sprintf('No se pudo encontrar el Torneo con id : %s', $id));
        }

        $positions = array();
        $teams = array();
        foreach ($tournament->getTeamsParticipation() as $teamInTournament) {
            $positions[$teamInTournament->getId()] = array('pj' => 0, 'pg' => 0, 'pe' => 0, 'pp' => 0, 'gf' => 0, 'gc' => 0, 'pts' => 0);
            $teams[$teamInTournament->getId()] = $teamInTournament;
        }

        foreach ($tournament->getRounds() as $round) {
            foreach ($round->getMatches() as $match) {
                if (MatchUtils::isMatchEnded($match)) {
                    $positions[$match->getLocal()->getId()]['pj'] ++;
                    $positions[$match->getVisitor()->getId()]['pj'] ++;

                    if (MatchUtils::isLocalWinner($match)) {
                        $positions[$match->getLocal()->getId()]['pg'] ++;
                        $positions[$match->getVisitor()->getId()]['pp'] ++;
                    } elseif (MatchUtils::isVisitorWinner($match)) {
                        $positions[$match->getVisitor()->getId()]['pg'] ++;
                        $positions[$match->getLocal()->getId()]['pp'] ++;
                    } else {
                        $positions[$match->getVisitor()->getId()]['pe'] ++;
                        $positions[$match->getLocal()->getId()]['pe'] ++;
                    }

                    $positions[$match->getVisitor()->getId()]['gf'] += $match->getVisitorGoals();
                    $positions[$match->getLocal()->getId()]['gf'] += $match->getLocalGoals();

                    $positions[$match->getVisitor()->getId()]['gc'] += $match->getLocalGoals();
                    $positions[$match->getLocal()->getId()]['gc'] += $match->getVisitorGoals();

                    $positions[$match->getVisitor()->getId()]['pts'] += MatchUtils::visitorPoints($match);
                    $positions[$match->getLocal()->getId()]['pts'] += MatchUtils::localPoints($match);
                }
            }
        }

        uasort($positions, function ($a, $b) {
            if ($a['pts'] === $b['pts']) {
                return 0;
            }
            return ($a['pts'] > $b['pts']) ? -1 : 1;
        });

        foreach ($positions as $key => $value) {
            $teamsInTournament[] = $teams[$key];
        }

        return array(
            'admin_pool' => $admin_pool,
            'teamsInTournament' => $teamsInTournament,
            'positions' => $positions
        );
    }

    public function inscriptionsDiscountAction($id)
    {
        $tournament = $this->admin->getObject($id);

        if (!$tournament) {
            throw new NotFoundHttpException(sprintf('No se pudo encontrar el Torneo con id : %s', $id));
        }

        $currentAccountManager = $this->get('tournament.manager.current_account');

        foreach ($tournament->getTeams() as $team) {
            $currentAccountManager->discountTournamentInscription($team, $tournament);
        }

        return new RedirectResponse($this->admin->generateUrl('show', array('id' => $tournament->getId())));
    }

    /**
     * @Template()
     */
    public function topScorersAction($id)
    {
        $admin_pool = $this->get('sonata.admin.pool');

        $tournament = $this->admin->getObject($id);
        if (!$tournament) {
            throw new NotFoundHttpException(sprintf('No se pudo encontrar el Torneo con id : %s', $id));
        }

        $qb = $this->getDoctrine()->getManager()
                ->getRepository('TournamentBundle:PlayerParticipation')
                ->createQueryBuilder('pp');

        $qb = $qb->select('pp,p,tmp,tt')
                ->innerJoin('pp.teamMatchParticipation', 'tmp')
                ->innerJoin('pp.player', 'p')
                ->innerJoin('tmp.teamInTournament', 'tt')
                ->innerJoin('tmp.playedFootballMatch', 'pfm')
                ->leftJoin('pfm.footballMatch', 'fm')
                ->leftJoin('fm.round', 'r')
                ->leftJoin('r.tournament', 'tournament')
                ->where('tournament = :tournament')
                ->setParameter('tournament', $tournament)
        ;

        $scorers = $qb->getQuery()->getResult();

        return array(
            'admin_pool' => $admin_pool,
            'scorers' => $scorers
        );
    }

}
