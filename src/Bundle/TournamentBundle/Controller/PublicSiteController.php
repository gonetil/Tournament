<?php

namespace Bundle\TournamentBundle\Controller;

use JMS\Serializer\SerializerBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

class PublicSiteController extends Controller
{

    /**
     * @Route("/", name="public_index")
     */
    public function indexAction()
    {
        
        return new Response('INDEX');
    }

}
