<?php

namespace Bundle\TournamentBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Bundle\TournamentBundle\Common\MatchUtils;

class TeamController extends Controller
{

    /**
     * @Template()
     */
    public function currentAccountAction($id)
    {
        $admin_pool = $this->get('sonata.admin.pool');

        $team = $this->admin->getObject($id);
        if (!$team) {
            throw new NotFoundHttpException(sprintf('No se pudo encontrar el Equipo con id : %s', $id));
        }

        $currentAccount = $team->getCurrentAccount();
        $entries = array();
        if (!is_null($currentAccount)) {
            $entries = $currentAccount->getEntries();
        }

        return array(
            'admin_pool' => $admin_pool,
            'current_account' => $currentAccount,
            'entries' => $entries
        );
    }

}
