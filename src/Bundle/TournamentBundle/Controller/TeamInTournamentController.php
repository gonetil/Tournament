<?php

namespace Bundle\TournamentBundle\Controller;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\JsonResponse;

class TeamInTournamentController extends DefaultController
{

    public function playersAction($id)
    {
        $teamInTournament = $this->admin->getModelManager()->find('TournamentBundle:TeamInTournament', $id);

        if (!$teamInTournament) {
            throw new NotFoundHttpException(sprintf('No se pudo encontrar el Equipo con id %s en el Torneo', $id));
        }

        return $this->serializeAndSendJSON($teamInTournament->getPlayers());
    }

}
