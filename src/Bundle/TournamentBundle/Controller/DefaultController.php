<?php

namespace Bundle\TournamentBundle\Controller;

use JMS\Serializer\SerializerBuilder;
use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Symfony\Component\HttpFoundation\JsonResponse;


class DefaultController extends Controller
{

    /**
     * @param $data  data to be serialized into JSON
     * @return JsonResponse $data data serialized
     */
    protected function serializeAndSendJSON($data)
    {
        $serializer = SerializerBuilder::create()->build();
        $jsonContent = $serializer->serialize($data,'json');
        return new JsonResponse($jsonContent);
    }

}
