<?php

namespace Bundle\TournamentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * Player
 *
 * @ORM\Table(name="player")
 * @ORM\Entity()
 * @ExclusionPolicy("all")
 */
class Player
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=50)
     * @Expose
     */
    private $lastname;

    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=50)
     * @Expose
     */
    private $firstname;

    /**
     * @var string
     * @ORM\Column(name="dni", type="string", length=20)
     * @Expose
     */
    private $dni;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birth", type="datetime")
     */
    private $birth;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     * @Expose
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="healthInfo", type="text", nullable=true)
     */
    private $healthInfo;

    /**
     * @ORM\ManyToOne(targetEntity="TeamInTournament", inversedBy="players")
     */
    private $teamInTournament;

    /**
     * @ORM\ManyToMany(targetEntity="TeamMatchParticipation", mappedBy="players")
     */
    private $played_matches;

    /**
     * @var integer
     * @ORM\Column(name="number", type="integer")
     * @Expose
     */
    private $number;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->played_matches = new \Doctrine\Common\Collections\ArrayCollection();
        $this->number = 0;
    }

    public function __toString()
    {
        return $this->getLastname() . ", " . $this->getFirstname();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set birth
     *
     * @param \DateTime $birth
     * @return Player
     */
    public function setBirth($birth)
    {
        $this->birth = $birth;

        return $this;
    }

    /**
     * Get birth
     *
     * @return \DateTime 
     */
    public function getBirth()
    {
        return $this->birth;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Player
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set healthInfo
     *
     * @param string $healthInfo
     * @return Player
     */
    public function setHealthInfo($healthInfo)
    {
        $this->healthInfo = $healthInfo;

        return $this;
    }

    /**
     * Get healthInfo
     *
     * @return string 
     */
    public function getHealthInfo()
    {
        return $this->healthInfo;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     * @return Player
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string 
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     * @return Player
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string 
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set dni
     *
     * @param string $dni
     * @return Player
     */
    public function setDni($dni)
    {
        $this->dni = $dni;

        return $this;
    }

    /**
     * Get dni
     *
     * @return string 
     */
    public function getDni()
    {
        return $this->dni;
    }

    /**
     * Add played_matches
     *
     * @param \Bundle\TournamentBundle\Entity\TeamMatchParticipation $playedMatches
     * @return Player
     */
    public function addPlayedMatch(\Bundle\TournamentBundle\Entity\TeamMatchParticipation $playedMatches)
    {
        $this->played_matches[] = $playedMatches;

        return $this;
    }

    /**
     * Remove played_matches
     *
     * @param \Bundle\TournamentBundle\Entity\TeamMatchParticipation $playedMatches
     */
    public function removePlayedMatch(\Bundle\TournamentBundle\Entity\TeamMatchParticipation $playedMatches)
    {
        $this->played_matches->removeElement($playedMatches);
    }

    /**
     * Get played_matches
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPlayedMatches()
    {
        return $this->played_matches;
    }

    /**
     * Set number
     *
     * @param integer $number
     * @return Player
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return integer 
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set teamInTournament
     *
     * @param \Bundle\TournamentBundle\Entity\TeamInTournament $teamInTournament
     *
     * @return Player
     */
    public function setTeamInTournament(\Bundle\TournamentBundle\Entity\TeamInTournament $teamInTournament = null)
    {
        $this->teamInTournament = $teamInTournament;

        return $this;
    }

    /**
     * Get teamInTournament
     *
     * @return \Bundle\TournamentBundle\Entity\TeamInTournament
     */
    public function getTeamInTournament()
    {
        return $this->teamInTournament;
    }
}
