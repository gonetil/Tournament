<?php

namespace Bundle\TournamentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TeamInTournament
 *
 * @ORM\Table(name="team_in_tournament")
 * @ORM\Entity()
 */
class TeamInTournament
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Team", inversedBy="tournamentParticipations")
     * @ORM\JoinColumn(name="team_id", referencedColumnName="id")
     */
    private $team;

    /**
     * @ORM\ManyToOne(targetEntity="Tournament", inversedBy="teamsParticipation")
     * @ORM\JoinColumn(name="tournament_id", referencedColumnName="id")
     */
    private $tournament;

    /**
     * @ORM\OneToMany(targetEntity="Player", mappedBy="teamInTournament", cascade={"persist"})
     */
    private $players;

    /**
     *  @ORM\OneToMany(targetEntity="FootballMatch", mappedBy="visitor")
     */
    private $visitor_matches;

    /**
     *  @ORM\OneToMany(targetEntity="FootballMatch", mappedBy="local")
     */
    private $local_matches;

    /**
     * @ORM\OneToMany(targetEntity="TeamMatchParticipation", mappedBy="teamInTournament")
     */
    private $teamMatchParticipations;

    /**
     * @ORM\OneToMany(targetEntity="Payment", mappedBy="teamInTournament")
     */
    private $payments;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->players = new \Doctrine\Common\Collections\ArrayCollection();
        $this->visitor_matches = new \Doctrine\Common\Collections\ArrayCollection();
        $this->local_matches = new \Doctrine\Common\Collections\ArrayCollection();
        $this->payments = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return $this->getTeam()->getName();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set players
     *
     * @param array $players
     * @return Team
     */
    public function setPlayers($players)
    {
        foreach ($players as $p) {
            $this->addPlayer($p);
        }

        return $this;
    }

    /**
     * Get players
     *
     * @return array 
     */
    public function getPlayers()
    {
        return $this->players;
    }

    /**
     * Set delegates
     *
     * @param array $delegates
     * @return Team
     */
    public function setDelegates($delegates)
    {
        foreach ($delegates as $d) {
            $this->addDelegate($d);
        }

        return $this;
    }

    /**
     * Get delegates
     *
     * @return array 
     */
    public function getDelegates()
    {
        return $this->delegates;
    }

    /**
     * Add players
     *
     * @param \Bundle\TournamentBundle\Entity\Player $players
     * @return Team
     */
    public function addPlayer(\Bundle\TournamentBundle\Entity\Player $player)
    {
        $player->setTeam($this);
        $this->players[] = $player;

        return $this;
    }

    /**
     * Remove players
     *
     * @param \Bundle\TournamentBundle\Entity\Player $players
     */
    public function removePlayer(\Bundle\TournamentBundle\Entity\Player $player)
    {
        $this->players->removeElement($player);
    }

    /**
     * Add delegates
     *
     * @param \Application\Sonata\UserBundle\Entity\User $delegates
     * @return Team
     */
    public function addDelegate(\Application\Sonata\UserBundle\Entity\User $delegate)
    {
        $delegate->setManagedTeam($this);
        $this->delegates[] = $delegate;

        return $this;
    }

    /**
     * Remove delegates
     *
     * @param \Application\Sonata\UserBundle\Entity\User $delegates
     */
    public function removeDelegate(\Application\Sonata\UserBundle\Entity\User $delegates)
    {
        $this->delegates->removeElement($delegates);
    }

    /**
     * Set visitor_matches
     *
     * @param \Bundle\TournamentBundle\Entity\FootballMatch $visitorMatches
     * @return Team
     */
    public function setVisitorMatches(\Bundle\TournamentBundle\Entity\FootballMatch $visitorMatches = null)
    {
        $this->visitor_matches = $visitorMatches;

        return $this;
    }

    /**
     * Get visitor_matches
     *
     * @return \Bundle\TournamentBundle\Entity\FootballMatch
     */
    public function getVisitorMatches()
    {
        return $this->visitor_matches;
    }

    /**
     * Set local_matches
     *
     * @param \Bundle\TournamentBundle\Entity\FootballMatch $localMatches
     * @return Team
     */
    public function setLocalMatches(\Bundle\TournamentBundle\Entity\FootballMatch $localMatches = null)
    {
        $this->local_matches = $localMatches;

        return $this;
    }

    /**
     * Get local_matches
     *
     * @return \Bundle\TournamentBundle\Entity\FootballMatch
     */
    public function getLocalMatches()
    {
        return $this->local_matches;
    }

    /**
     * Set team
     *
     * @param \Bundle\TournamentBundle\Entity\Team $team
     *
     * @return TeamInTournament
     */
    public function setTeam(\Bundle\TournamentBundle\Entity\Team $team = null)
    {
        $this->team = $team;

        return $this;
    }

    /**
     * Get team
     *
     * @return \Bundle\TournamentBundle\Entity\Team
     */
    public function getTeam()
    {
        return $this->team;
    }

    /**
     * Set tournament
     *
     * @param \Bundle\TournamentBundle\Entity\Tournament $tournament
     *
     * @return TeamInTournament
     */
    public function setTournament(\Bundle\TournamentBundle\Entity\Tournament $tournament = null)
    {
        $this->tournament = $tournament;

        return $this;
    }

    /**
     * Get tournament
     *
     * @return \Bundle\TournamentBundle\Entity\Tournament
     */
    public function getTournament()
    {
        return $this->tournament;
    }

    /**
     * Add visitorMatch
     *
     * @param \Bundle\TournamentBundle\Entity\FootballMatch $visitorMatch
     *
     * @return TeamInTournament
     */
    public function addVisitorMatch(\Bundle\TournamentBundle\Entity\FootballMatch $visitorMatch)
    {
        $this->visitor_matches[] = $visitorMatch;

        return $this;
    }

    /**
     * Remove visitorMatch
     *
     * @param \Bundle\TournamentBundle\Entity\FootballMatch $visitorMatch
     */
    public function removeVisitorMatch(\Bundle\TournamentBundle\Entity\FootballMatch $visitorMatch)
    {
        $this->visitor_matches->removeElement($visitorMatch);
    }

    /**
     * Add localMatch
     *
     * @param \Bundle\TournamentBundle\Entity\FootballMatch $localMatch
     *
     * @return TeamInTournament
     */
    public function addLocalMatch(\Bundle\TournamentBundle\Entity\FootballMatch $localMatch)
    {
        $this->local_matches[] = $localMatch;

        return $this;
    }

    /**
     * Remove localMatch
     *
     * @param \Bundle\TournamentBundle\Entity\FootballMatch $localMatch
     */
    public function removeLocalMatch(\Bundle\TournamentBundle\Entity\FootballMatch $localMatch)
    {
        $this->local_matches->removeElement($localMatch);
    }

    /**
     * Add teamMatchParticipation
     *
     * @param \Bundle\TournamentBundle\Entity\TeamMatchParticipation $teamMatchParticipation
     *
     * @return TeamInTournament
     */
    public function addTeamMatchParticipation(\Bundle\TournamentBundle\Entity\TeamMatchParticipation $teamMatchParticipation)
    {
        $this->teamMatchParticipations[] = $teamMatchParticipation;

        return $this;
    }

    /**
     * Remove teamMatchParticipation
     *
     * @param \Bundle\TournamentBundle\Entity\TeamMatchParticipation $teamMatchParticipation
     */
    public function removeTeamMatchParticipation(\Bundle\TournamentBundle\Entity\TeamMatchParticipation $teamMatchParticipation)
    {
        $this->teamMatchParticipations->removeElement($teamMatchParticipation);
    }

    /**
     * Get teamMatchParticipations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTeamMatchParticipations()
    {
        return $this->teamMatchParticipations;
    }

    /**
     * Add payment
     *
     * @param \Bundle\TournamentBundle\Entity\Payment $payment
     *
     * @return TeamInTournament
     */
    public function addPayment(\Bundle\TournamentBundle\Entity\Payment $payment)
    {
        $this->payments[] = $payment;

        return $this;
    }

    /**
     * Remove payment
     *
     * @param \Bundle\TournamentBundle\Entity\Payment $payment
     */
    public function removePayment(\Bundle\TournamentBundle\Entity\Payment $payment)
    {
        $this->payments->removeElement($payment);
    }

    /**
     * Get payments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPayments()
    {
        return $this->payments;
    }

}
