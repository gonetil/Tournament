<?php

namespace Bundle\TournamentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PlayedFootballMatchReason
 * 
 * @ORM\Entity
 */
class PlayedFootballMatchEntry extends CurrentAccountEntry
{

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="PlayedFootballMatch")
     * @ORM\JoinColumn(name="played_football_match_id", referencedColumnName="id")
     * */
    private $playedFootballMatch;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set playedFootballMatch
     *
     * @param \Bundle\TournamentBundle\Entity\PlayedFootballMatch $playedFootballMatch
     *
     * @return PlayedFootballMatchEntry
     */
    public function setPlayedFootballMatch(\Bundle\TournamentBundle\Entity\PlayedFootballMatch $playedFootballMatch = null)
    {
        $this->playedFootballMatch = $playedFootballMatch;

        return $this;
    }

    /**
     * Get playedFootballMatch
     *
     * @return \Bundle\TournamentBundle\Entity\PlayedFootballMatch
     */
    public function getPlayedFootballMatch()
    {
        return $this->playedFootballMatch;
    }

}
