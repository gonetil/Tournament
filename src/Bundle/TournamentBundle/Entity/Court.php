<?php

namespace Bundle\TournamentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Court
 *
 * @ORM\Table(name="court")
 * @ORM\Entity
 */
class Court
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100)
     */
    private $name;

    /**
     * @var boolean
     *
     * @ORM\Column(name="enabled", type="boolean")
     */
    private $enabled;

    /**
     * @ORM\OneToMany(targetEntity="FootballMatch", mappedBy="court")
     */
    private $played_matches;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->played_matches = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return $this->getName();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Court
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     * @return Court
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean 
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set played_matches
     *
     * @param \Bundle\TournamentBundle\Entity\FootballMatch $playedMatches
     * @return Court
     */
    public function setPlayedMatches(\Bundle\TournamentBundle\Entity\FootballMatch $playedMatches = null)
    {
        $this->played_matches = $playedMatches;

        return $this;
    }

    /**
     * Get played_matches
     *
     * @return \Bundle\TournamentBundle\Entity\FootballMatch
     */
    public function getPlayedMatches()
    {
        return $this->played_matches;
    }

    /**
     * Add playedMatch
     *
     * @param \Bundle\TournamentBundle\Entity\FootballMatch $playedMatch
     *
     * @return Court
     */
    public function addPlayedMatch(\Bundle\TournamentBundle\Entity\FootballMatch $playedMatch)
    {
        $this->played_matches[] = $playedMatch;

        return $this;
    }

    /**
     * Remove playedMatch
     *
     * @param \Bundle\TournamentBundle\Entity\FootballMatch $playedMatch
     */
    public function removePlayedMatch(\Bundle\TournamentBundle\Entity\FootballMatch $playedMatch)
    {
        $this->played_matches->removeElement($playedMatch);
    }

}
