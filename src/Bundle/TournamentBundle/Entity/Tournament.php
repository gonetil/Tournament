<?php

namespace Bundle\TournamentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tournament
 *
 * @ORM\Table(name="tournament")
 * @ORM\Entity(repositoryClass = "Bundle\TournamentBundle\Repository\TournamentRepository")
 */
class Tournament
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="TournamentType")
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity="TournamentStatus")
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isRegistrationOpen", type="boolean", nullable=true)
     */
    private $isRegistrationOpen;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateBegin", type="datetime")
     */
    private $dateBegin;

    /**
     * @var dateTime
     *
     * @ORM\Column(name="dateEnd", type="datetime")
     */
    private $dateEnd;

    /**
     * @var float
     *
     * @ORM\Column(name="inscriptionCostPerTeam", type="float")
     */
    private $inscriptionCostPerTeam;

    /**
     * @var float
     *
     * @ORM\Column(name="matchCostPerTeam", type="float")
     */
    private $matchCostPerTeam;

    /**
     * @var array
     *
     * @ORM\ManyToMany(targetEntity="Sponsor")
     */
    private $sponsors;

    /**
     * @var array
     *
     * @ORM\ManyToMany(targetEntity="Team")
     */
    private $teams;

    /**
     * @ORM\OneToMany(targetEntity="TeamInTournament", mappedBy="tournament", cascade={"persist","remove"})
     */
    private $teamsParticipation;

    /**
     * @var Round
     * @ORM\OneToMany(targetEntity="Round", mappedBy="tournament", cascade={"persist","remove"})
     */
    private $rounds;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->sponsors = new \Doctrine\Common\Collections\ArrayCollection();
        $this->teams = new \Doctrine\Common\Collections\ArrayCollection();
        $this->rounds = new \Doctrine\Common\Collections\ArrayCollection();
        $this->teamsParticipation = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return "{$this->getName()} ({$this->getType()} - {$this->getStatus()}) ";
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Tournament
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set isRegistrationOpen
     *
     * @param $isRegistrationOpen
     * @return Tournament
     */
    public function setIsRegistrationOpen($isRegistrationOpen)
    {
        $this->isRegistrationOpen = $isRegistrationOpen;

        return $this;
    }

    /**
     * Get isRegistrationOpen
     *
     * @return \boolean
     */
    public function getIsRegistrationOpen()
    {
        return $this->isRegistrationOpen;
    }

    /**
     * Set dateBegin
     *
     * @param \DateTime $dateBegin
     * @return Tournament
     */
    public function setDateBegin($dateBegin)
    {
        $this->dateBegin = $dateBegin;

        return $this;
    }

    /**
     * Get dateBegin
     *
     * @return \DateTime 
     */
    public function getDateBegin()
    {
        return $this->dateBegin;
    }

    /**
     * Set dateEnd
     *
     * @param \DateTime $dateEnd
     * @return Tournament
     */
    public function setDateEnd(\dateTime $dateEnd)
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    /**
     * Get dateEnd
     *
     * @return \DateTime
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * Set inscriptionCostPerTeam
     *
     * @param float $inscriptionCostPerTeam
     * @return Tournament
     */
    public function setInscriptionCostPerTeam($inscriptionCostPerTeam)
    {
        $this->inscriptionCostPerTeam = $inscriptionCostPerTeam;

        return $this;
    }

    /**
     * Get inscriptionCostPerTeam
     *
     * @return float 
     */
    public function getInscriptionCostPerTeam()
    {
        return $this->inscriptionCostPerTeam;
    }

    /**
     * Set matchCostPerTeam
     *
     * @param float $matchCostPerTeam
     * @return Tournament
     */
    public function setMatchCostPerTeam($matchCostPerTeam)
    {
        $this->matchCostPerTeam = $matchCostPerTeam;

        return $this;
    }

    /**
     * Get matchCostPerTeam
     *
     * @return float 
     */
    public function getMatchCostPerTeam()
    {
        return $this->matchCostPerTeam;
    }

    /**
     * Add sponsors
     *
     * @param \Bundle\TournamentBundle\Entity\Sponsor $sponsors
     * @return Tournament
     */
    public function addSponsor(\Bundle\TournamentBundle\Entity\Sponsor $sponsors)
    {
        $this->sponsors[] = $sponsors;

        return $this;
    }

    /**
     * Remove sponsors
     *
     * @param \Bundle\TournamentBundle\Entity\Sponsor $sponsors
     */
    public function removeSponsor(\Bundle\TournamentBundle\Entity\Sponsor $sponsors)
    {
        $this->sponsors->removeElement($sponsors);
    }

    /**
     * Get sponsors
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSponsors()
    {
        return $this->sponsors;
    }

    /**
     * Add rounds
     *
     * @param \Bundle\TournamentBundle\Entity\Round $rounds
     * @return Tournament
     */
    public function addRound(\Bundle\TournamentBundle\Entity\Round $rounds)
    {
        $this->rounds[] = $rounds;

        return $this;
    }

    /**
     * Remove rounds
     *
     * @param \Bundle\TournamentBundle\Entity\Round $rounds
     */
    public function removeRound(\Bundle\TournamentBundle\Entity\Round $rounds)
    {
        $this->rounds->removeElement($rounds);
    }

    /**
     * Get rounds
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRounds()
    {
        return $this->rounds;
    }

    /**
     * Set type
     *
     * @param \Bundle\TournamentBundle\Entity\TournamentType $type
     * @return Tournament
     */
    public function setType(\Bundle\TournamentBundle\Entity\TournamentType $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \Bundle\TournamentBundle\Entity\TournamentType 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set status
     *
     * @param \Bundle\TournamentBundle\Entity\TournamentStatus $status
     * @return Tournament
     */
    public function setStatus(\Bundle\TournamentBundle\Entity\TournamentStatus $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return \Bundle\TournamentBundle\Entity\TournamentStatus 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Add teamsParticipation
     *
     * @param \Bundle\TournamentBundle\Entity\TeamInTournament $teamParticipation
     *
     * @return Tournament
     */
    public function addTeamParticipation(\Bundle\TournamentBundle\Entity\TeamInTournament $teamParticipation)
    {
        $this->teamsParticipation[] = $teamParticipation;

        return $this;
    }

    /**
     * Remove teamsParticipation
     *
     * @param \Bundle\TournamentBundle\Entity\TeamInTournament $teamParticipation
     */
    public function removeTeamParticipation(\Bundle\TournamentBundle\Entity\TeamInTournament $teamParticipation)
    {
        $this->teamsParticipation->removeElement($teamParticipation);
    }

    /**
     * Get teamsParticipation
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTeamsParticipation()
    {
        return $this->teamsParticipation;
    }

    /**
     * Add team
     *
     * @param \Bundle\TournamentBundle\Entity\Team $team
     *
     * @return Tournament
     */
    public function addTeam(\Bundle\TournamentBundle\Entity\Team $team)
    {
        $this->teams[] = $team;

        return $this;
    }

    /**
     * Remove team
     *
     * @param \Bundle\TournamentBundle\Entity\Team $team
     */
    public function removeTeam(\Bundle\TournamentBundle\Entity\Team $team)
    {
        $this->teams->removeElement($team);
    }

    /**
     * Get teams
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTeams()
    {
        return $this->teams;
    }

    /**
     * Add teamsParticipation
     *
     * @param \Bundle\TournamentBundle\Entity\TeamInTournament $teamsParticipation
     *
     * @return Tournament
     */
    public function addTeamsParticipation(\Bundle\TournamentBundle\Entity\TeamInTournament $teamsParticipation)
    {
        $this->teamsParticipation[] = $teamsParticipation;

        return $this;
    }

    /**
     * Remove teamsParticipation
     *
     * @param \Bundle\TournamentBundle\Entity\TeamInTournament $teamsParticipation
     */
    public function removeTeamsParticipation(\Bundle\TournamentBundle\Entity\TeamInTournament $teamsParticipation)
    {
        $this->teamsParticipation->removeElement($teamsParticipation);
    }

}
