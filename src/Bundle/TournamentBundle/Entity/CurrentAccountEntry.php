<?php

namespace Bundle\TournamentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="current_account_entry")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="entry_type", type="string")
 * @ORM\DiscriminatorMap({
 *      "current_account_entry" = "CurrentAccountEntry",
 *      "tournament_inscription" = "TournamentInscriptionEntry",
 *      "played_football_match" = "PlayedFootballMatchEntry"
 * })
 */
class CurrentAccountEntry
{

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="CurrentAccount", inversedBy="entries")
     * @ORM\JoinColumn(name="current_account_id", referencedColumnName="id")
     */
    private $currentAccount;

    /**
     * @ORM\Column(name="date", type="date")
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity="Reason")
     * @ORM\JoinColumn(name="reason", referencedColumnName="id")
     */
    private $reason;

    /**
     * @ORM\Column(name="amount", type="float")
     */
    private $amount;

    /**
     * @ORM\Column(name="description", type="string", nullable=true)
     */
    private $description;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return CurrentAccountEntry
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set amount
     *
     * @param float $amount
     *
     * @return CurrentAccountEntry
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return CurrentAccountEntry
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set currentAccount
     *
     * @param \Bundle\TournamentBundle\Entity\CurrentAccount $currentAccount
     *
     * @return CurrentAccountEntry
     */
    public function setCurrentAccount(\Bundle\TournamentBundle\Entity\CurrentAccount $currentAccount = null)
    {
        $this->currentAccount = $currentAccount;

        return $this;
    }

    /**
     * Get currentAccount
     *
     * @return \Bundle\TournamentBundle\Entity\CurrentAccount
     */
    public function getCurrentAccount()
    {
        return $this->currentAccount;
    }

    /**
     * Set reason
     *
     * @param \Bundle\TournamentBundle\Entity\Reason $reason
     *
     * @return CurrentAccountEntry
     */
    public function setReason(\Bundle\TournamentBundle\Entity\Reason $reason = null)
    {
        $this->reason = $reason;

        return $this;
    }

    /**
     * Get reason
     *
     * @return \Bundle\TournamentBundle\Entity\Reason
     */
    public function getReason()
    {
        return $this->reason;
    }

}
