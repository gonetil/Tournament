<?php

namespace Bundle\TournamentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="player_participation")
 * @ORM\Entity()
 */
class PlayerParticipation
{

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Player")
     * @ORM\JoinColumn(name="player", referencedColumnName="id")
     */
    private $player;

    /**
     * @ORM\Column(name="yellow_cards", type="integer", nullable=true)
     */
    private $yellowCards = 0;

    /**
     * @ORM\Column(name="red_cards", type="integer", nullable=true)
     */
    private $redCards = 0;

    /**
     * @ORM\Column(name="goals", type="integer", nullable=true)
     */
    private $goals = 0;

    /**
     * @ORM\Column(name="suspended_dates", type="integer", nullable=true)
     */
    private $suspendedDates = 0;

    /**
     * @ORM\Column(name="tshirt_number", type="integer", nullable=true)
     */
    private $tshirtNumber;

    /**
     * @ORM\ManyToOne(targetEntity="TeamMatchParticipation", inversedBy="playersParticipation")
     * @ORM\JoinColumn(name="team_match_participation", referencedColumnName="id")
     * */
    private $teamMatchParticipation;

    function __toString()
    {
        return $this->getPlayer()->getLastname() . ', ' . $this->getPlayer()->getFirstname();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set yellowCards
     *
     * @param integer $yellowCards
     *
     * @return PlayerParticipation
     */
    public function setYellowCards($yellowCards)
    {
        $this->yellowCards = $yellowCards;

        return $this;
    }

    /**
     * Get yellowCards
     *
     * @return integer
     */
    public function getYellowCards()
    {
        return $this->yellowCards;
    }

    /**
     * Set redCards
     *
     * @param integer $redCards
     *
     * @return PlayerParticipation
     */
    public function setRedCards($redCards)
    {
        $this->redCards = $redCards;

        return $this;
    }

    /**
     * Get redCards
     *
     * @return integer
     */
    public function getRedCards()
    {
        return $this->redCards;
    }

    /**
     * Set goals
     *
     * @param integer $goals
     *
     * @return PlayerParticipation
     */
    public function setGoals($goals)
    {
        $this->goals = $goals;

        return $this;
    }

    /**
     * Get goals
     *
     * @return integer
     */
    public function getGoals()
    {
        return $this->goals;
    }

    /**
     * Set suspendedDates
     *
     * @param integer $suspendedDates
     *
     * @return PlayerParticipation
     */
    public function setSuspendedDates($suspendedDates)
    {
        $this->suspendedDates = $suspendedDates;

        return $this;
    }

    /**
     * Get suspendedDates
     *
     * @return integer
     */
    public function getSuspendedDates()
    {
        return $this->suspendedDates;
    }

    /**
     * Set tshirtNumber
     *
     * @param integer $tshirtNumber
     *
     * @return PlayerParticipation
     */
    public function setTshirtNumber($tshirtNumber)
    {
        $this->tshirtNumber = $tshirtNumber;

        return $this;
    }

    /**
     * Get tshirtNumber
     *
     * @return integer
     */
    public function getTshirtNumber()
    {
        return $this->tshirtNumber;
    }

    /**
     * Set teamMatchParticipation
     *
     * @param \Bundle\TournamentBundle\Entity\TeamMatchParticipation $teamMatchParticipation
     *
     * @return PlayerParticipation
     */
    public function setTeamMatchParticipation(\Bundle\TournamentBundle\Entity\TeamMatchParticipation $teamMatchParticipation = null)
    {
        $this->teamMatchParticipation = $teamMatchParticipation;

        return $this;
    }

    /**
     * Get teamMatchParticipation
     *
     * @return \Bundle\TournamentBundle\Entity\TeamMatchParticipation
     */
    public function getTeamMatchParticipation()
    {
        return $this->teamMatchParticipation;
    }

    /**
     * Set player
     *
     * @param \Bundle\TournamentBundle\Entity\Player $player
     *
     * @return PlayerParticipation
     */
    public function setPlayer(\Bundle\TournamentBundle\Entity\Player $player = null)
    {
        $this->player = $player;

        return $this;
    }

    /**
     * Get player
     *
     * @return \Bundle\TournamentBundle\Entity\Player
     */
    public function getPlayer()
    {
        return $this->player;
    }

}
