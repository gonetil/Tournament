<?php

namespace Bundle\TournamentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="payment")
 * @ORM\Entity()
 */
class Payment
{

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="TeamInTournament", inversedBy="payments")
     * @ORM\JoinColumn(name="team_in_tournament", referencedColumnName="id")
     */
    private $teamInTournament;

    /**
     * @ORM\Column(name="date", type="date")
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity="PaymentReason")
     * @ORM\JoinColumn(name="payment_reason", referencedColumnName="id")
     */
    private $reason;

    /**
     * @ORM\Column(name="amount", type="float")
     */
    private $amount;

    /**
     * @ORM\Column(name="description", type="string")
     */
    private $description;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Payment
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set amount
     *
     * @param float $amount
     *
     * @return Payment
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Payment
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set teamInTournament
     *
     * @param \Bundle\TournamentBundle\Entity\TeamInTournament $teamInTournament
     *
     * @return Payment
     */
    public function setTeamInTournament(\Bundle\TournamentBundle\Entity\TeamInTournament $teamInTournament = null)
    {
        $this->teamInTournament = $teamInTournament;

        return $this;
    }

    /**
     * Get teamInTournament
     *
     * @return \Bundle\TournamentBundle\Entity\TeamInTournament
     */
    public function getTeamInTournament()
    {
        return $this->teamInTournament;
    }

    /**
     * Set reason
     *
     * @param \Bundle\TournamentBundle\Entity\PaymentReason $reason
     *
     * @return Payment
     */
    public function setReason(\Bundle\TournamentBundle\Entity\PaymentReason $reason = null)
    {
        $this->reason = $reason;

        return $this;
    }

    /**
     * Get reason
     *
     * @return \Bundle\TournamentBundle\Entity\PaymentReason
     */
    public function getReason()
    {
        return $this->reason;
    }

}
