<?php

namespace Bundle\TournamentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PlayedFootballMatch
 *
 * @ORM\Table(name="played_football_match")
 * @ORM\Entity
 */
class PlayedFootballMatch
{

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="dateAndTime", type="datetime", nullable=true)
     */
    private $dateAndTime;

    /**
     *  @ORM\Column(name="court", type="string", nullable=true)
     */
    private $court;

    /**
     * @ORM\Column(name="referee", type="string", nullable=true)
     */
    private $referee;

    /**
     * @ORM\Column(name="refereeObservations", type="text", nullable=true)
     */
    private $refereeObservations;

    /**
     * @ORM\OneToOne(targetEntity="FootballMatch")
     * @ORM\JoinColumn(name="football_match", referencedColumnName="id")
     */
    private $footballMatch;

    /**
     * @ORM\OneToOne(targetEntity="TeamMatchParticipation")
     * @ORM\JoinColumn(name="local_participation", referencedColumnName="id")
     */
    private $local_participation;

    /**
     * @ORM\OneToOne(targetEntity="TeamMatchParticipation")
     * @ORM\JoinColumn(name="visitor_participation", referencedColumnName="id")
     */
    private $visitor_participation;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set footballMatch
     *
     * @param \Bundle\TournamentBundle\Entity\FootballMatch $footballMatch
     *
     * @return PlayedFootballMatch
     */
    public function setFootballMatch(\Bundle\TournamentBundle\Entity\FootballMatch $footballMatch = null)
    {
        $this->footballMatch = $footballMatch;

        return $this;
    }

    /**
     * Get footballMatch
     *
     * @return \Bundle\TournamentBundle\Entity\FootballMatch
     */
    public function getFootballMatch()
    {
        return $this->footballMatch;
    }

    /**
     * Set localParticipation
     *
     * @param \Bundle\TournamentBundle\Entity\TeamMatchParticipation $localParticipation
     *
     * @return PlayedFootballMatch
     */
    public function setLocalParticipation(\Bundle\TournamentBundle\Entity\TeamMatchParticipation $localParticipation = null)
    {
        $this->local_participation = $localParticipation;

        return $this;
    }

    /**
     * Get localParticipation
     *
     * @return \Bundle\TournamentBundle\Entity\TeamMatchParticipation
     */
    public function getLocalParticipation()
    {
        return $this->local_participation;
    }

    /**
     * Set visitorParticipation
     *
     * @param \Bundle\TournamentBundle\Entity\TeamMatchParticipation $visitorParticipation
     *
     * @return PlayedFootballMatch
     */
    public function setVisitorParticipation(\Bundle\TournamentBundle\Entity\TeamMatchParticipation $visitorParticipation = null)
    {
        $this->visitor_participation = $visitorParticipation;

        return $this;
    }

    /**
     * Get visitorParticipation
     *
     * @return \Bundle\TournamentBundle\Entity\TeamMatchParticipation
     */
    public function getVisitorParticipation()
    {
        return $this->visitor_participation;
    }


    /**
     * Set dateAndTime
     *
     * @param \DateTime $dateAndTime
     *
     * @return PlayedFootballMatch
     */
    public function setDateAndTime($dateAndTime)
    {
        $this->dateAndTime = $dateAndTime;

        return $this;
    }

    /**
     * Get dateAndTime
     *
     * @return \DateTime
     */
    public function getDateAndTime()
    {
        return $this->dateAndTime;
    }

    /**
     * Set court
     *
     * @param string $court
     *
     * @return PlayedFootballMatch
     */
    public function setCourt($court)
    {
        $this->court = $court;

        return $this;
    }

    /**
     * Get court
     *
     * @return string
     */
    public function getCourt()
    {
        return $this->court;
    }

    /**
     * Set referee
     *
     * @param string $referee
     *
     * @return PlayedFootballMatch
     */
    public function setReferee($referee)
    {
        $this->referee = $referee;

        return $this;
    }

    /**
     * Get referee
     *
     * @return string
     */
    public function getReferee()
    {
        return $this->referee;
    }

    /**
     * Set refereeObservations
     *
     * @param string $refereeObservations
     *
     * @return PlayedFootballMatch
     */
    public function setRefereeObservations($refereeObservations)
    {
        $this->refereeObservations = $refereeObservations;

        return $this;
    }

    /**
     * Get refereeObservations
     *
     * @return string
     */
    public function getRefereeObservations()
    {
        return $this->refereeObservations;
    }

    public function __toString() {
        return ($match=$this->getFootballMatch()) ?$match->__toString() : "";
    }
}
