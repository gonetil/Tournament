<?php

namespace Bundle\TournamentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TeamMatchParticipation
 *
 * @ORM\Table(name="team_match_participation")
 * @ORM\Entity
 */
class TeamMatchParticipation
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=true)
     */
    private $date;

    /**
     * @var float
     *
     * @ORM\Column(name="payment", type="float", nullable=true)
     */
    private $payment;

    /**
     *
     * @ORM\ManyToOne(targetEntity="TeamInTournament", inversedBy="teamMatchParticipations")
     * @ORM\JoinColumn(name="team_in_tournament", referencedColumnName="id")
     */
    private $teamInTournament;

    /**
     * @ORM\ManyToOne(targetEntity="FootballMatch")
     * @ORM\JoinColumn(name="football_match", referencedColumnName="id")
     */
    private $footballMatch;

    /**
     * @ORM\ManyToOne(targetEntity="PlayedFootballMatch")
     * @ORM\JoinColumn(name="played_football_match", referencedColumnName="id")
     */
    private $playedFootballMatch;

    /**
     * @var integer
     *
     * @ORM\Column(name="goals_for", type="integer", nullable=true)
     */
    private $goals_for;

    /**
     * @var integer
     *
     * @ORM\Column(name="goals_against", type="integer", nullable=true)
     */
    private $goals_against;

    /**
     * @var boolean
     *
     * @ORM\Column(name="winner", type="boolean", nullable=true)
     */
    private $is_winner;

    /**
     * @var integer
     *
     * @ORM\Column(name="points", type="integer", nullable=true)
     */
    private $points;

    /**
     * @ORM\ManyToMany(targetEntity="Player", inversedBy="played_matches")
     */
    private $players;

    /**
     * @ORM\OneToMany(targetEntity="PlayerParticipation", mappedBy="teamMatchParticipation")
     */
    private $playersParticipation;

    /**
     * @ORM\OneToMany(targetEntity="PlayerChange", mappedBy="teamMatchParticipation")
     */
    private $playerChanges;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->players = new \Doctrine\Common\Collections\ArrayCollection();
        $this->playersParticipation = new \Doctrine\Common\Collections\ArrayCollection();
        $this->playerChanges = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return (!is_null($this->getTeamInTournament())) ?
                $this->getTeamInTournament()->getTeam()->getName() : '';
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return TeamMatchParticipation
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set payment
     *
     * @param float $payment
     * @return TeamMatchParticipation
     */
    public function setPayment($payment)
    {
        $this->payment = $payment;

        return $this;
    }

    /**
     * Get payment
     *
     * @return float 
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * Set goals_for
     *
     * @param integer $goalsFor
     * @return TeamMatchParticipation
     */
    public function setGoalsFor($goalsFor)
    {
        $this->goals_for = $goalsFor;

        return $this;
    }

    /**
     * Get goals_for
     *
     * @return integer 
     */
    public function getGoalsFor()
    {
        return $this->goals_for;
    }

    /**
     * Set goals_against
     *
     * @param integer $goalsAgainst
     * @return TeamMatchParticipation
     */
    public function setGoalsAgainst($goalsAgainst)
    {
        $this->goals_against = $goalsAgainst;

        return $this;
    }

    /**
     * Get goals_against
     *
     * @return integer
     */
    public function getGoalsAgainst()
    {
        return $this->goals_against;
    }

    /**
     * Set points
     *
     * @param integer $points
     * @return TeamMatchParticipation
     */
    public function setPoints($points)
    {
        $this->points = $points;

        return $this;
    }

    /**
     * Get points
     *
     * @return integer 
     */
    public function getPoints()
    {
        return $this->points;
    }

    /**
     * Set is_winner
     *
     * @param boolean $isWinner
     * @return TeamMatchParticipation
     */
    public function setIsWinner($isWinner)
    {
        $this->is_winner = $isWinner;

        return $this;
    }

    /**
     * Get is_winner
     *
     * @return boolean 
     */
    public function getIsWinner()
    {
        return $this->is_winner;
    }

    /**
     * Set team
     *
     * @param \Bundle\TournamentBundle\Entity\Team $team
     * @return TeamMatchParticipation
     */
    public function setTeam(\Bundle\TournamentBundle\Entity\Team $team = null)
    {
        $this->team = $team;

        return $this;
    }

    /**
     * Get team
     *
     * @return \Bundle\TournamentBundle\Entity\Team 
     */
    public function getTeam()
    {
        return $this->team;
    }

    /**
     * Add players
     *
     * @param \Bundle\TournamentBundle\Entity\Player $players
     * @return TeamMatchParticipation
     */
    public function addPlayer(\Bundle\TournamentBundle\Entity\Player $players)
    {
        $this->players[] = $players;

        return $this;
    }

    /**
     * Remove players
     *
     * @param \Bundle\TournamentBundle\Entity\Player $players
     */
    public function removePlayer(\Bundle\TournamentBundle\Entity\Player $players)
    {
        $this->players->removeElement($players);
    }

    /**
     * Get players
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPlayers()
    {
        return $this->players;
    }

    function getTeamInTournament()
    {
        return $this->teamInTournament;
    }

    function setTeamInTournament($teamInTournament)
    {
        $this->teamInTournament = $teamInTournament;

        return $this;
    }

    /**
     * Set footballMatch
     *
     * @param \Bundle\TournamentBundle\Entity\FootballMatch $footballMatch
     *
     * @return TeamMatchParticipation
     */
    public function setFootballMatch(\Bundle\TournamentBundle\Entity\FootballMatch $footballMatch = null)
    {
        $this->footballMatch = $footballMatch;

        return $this;
    }

    /**
     * Get footballMatch
     *
     * @return \Bundle\TournamentBundle\Entity\FootballMatch
     */
    public function getFootballMatch()
    {
        return $this->footballMatch;
    }

    /**
     * Add playersParticipation
     *
     * @param \Bundle\TournamentBundle\Entity\PlayerParticipation $playersParticipation
     *
     * @return TeamMatchParticipation
     */
    public function addPlayersParticipation(\Bundle\TournamentBundle\Entity\PlayerParticipation $playerParticipation)
    {
        $this->playersParticipation[] = $playerParticipation;

        return $this;
    }

    /**
     * Remove playersParticipation
     *
     * @param \Bundle\TournamentBundle\Entity\PlayerParticipation $playersParticipation
     */
    public function removePlayersParticipation(\Bundle\TournamentBundle\Entity\PlayerParticipation $playerParticipation)
    {
        $this->playersParticipation->removeElement($playerParticipation);
    }

    /**
     * Get playersParticipation
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlayersParticipation()
    {
        return $this->playersParticipation;
    }

    /**
     * Set playedFootballMatch
     *
     * @param \Bundle\TournamentBundle\Entity\FootballMatch $playedFootballMatch
     *
     * @return TeamMatchParticipation
     */
    public function setPlayedFootballMatch(\Bundle\TournamentBundle\Entity\PlayedFootballMatch $playedFootballMatch = null)
    {
        $this->playedFootballMatch = $playedFootballMatch;

        return $this;
    }

    /**
     * Get playedFootballMatch
     *
     * @return \Bundle\TournamentBundle\Entity\FootballMatch
     */
    public function getPlayedFootballMatch()
    {
        return $this->playedFootballMatch;
    }

    /**
     * Add playerChange
     *
     * @param \Bundle\TournamentBundle\Entity\PlayerChange $playerChange
     *
     * @return TeamMatchParticipation
     */
    public function addPlayerChange(\Bundle\TournamentBundle\Entity\PlayerChange $playerChange)
    {
        $this->playerChanges[] = $playerChange;

        return $this;
    }

    /**
     * Remove playerChange
     *
     * @param \Bundle\TournamentBundle\Entity\PlayerChange $playerChange
     */
    public function removePlayerChange(\Bundle\TournamentBundle\Entity\PlayerChange $playerChange)
    {
        $this->playerChanges->removeElement($playerChange);
    }

    /**
     * Get playerChanges
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlayerChanges()
    {
        return $this->playerChanges;
    }

}
