<?php

namespace Bundle\TournamentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Round
 *
 * @ORM\Table(name="round")
 * @ORM\Entity
 */
class Round
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="number", type="string", length=10)
     */
    private $number;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="estimatedDate", type="datetime", nullable=true)
     */
    private $estimatedDate;

    /**
     *
     * @ORM\OneToMany(targetEntity="FootballMatch", mappedBy="round",cascade={"persist","remove"})
     */
    private $matches;

    /**
     * @ORM\ManyToOne(targetEntity="Tournament", inversedBy="rounds")
     * @ORM\JoinColumn(name="tournament", referencedColumnName="id", nullable=false)
     */
    private $tournament;

    /**
     * @ORM\ManyToOne(targetEntity="RoundStatus")
     * @ORM\JoinColumn(name="round_status", referencedColumnName="id")
     */
    private $roundStatus;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->matches = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return "Fecha " . $this->getNumber() . ", " . $this->getTournament();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set number
     *
     * @param string $number
     * @return Round
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string 
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set estimatedDate
     *
     * @param \DateTime $estimatedDate
     * @return Round
     */
    public function setEstimatedDate($estimatedDate)
    {
        $this->estimatedDate = $estimatedDate;

        return $this;
    }

    /**
     * Get estimatedDate
     *
     * @return \DateTime 
     */
    public function getEstimatedDate()
    {
        return $this->estimatedDate;
    }

    /**
     * Set matches
     *
     * @param array $matches
     * @return Round
     */
    public function setMatches($matches)
    {
        $this->matches = $matches;

        return $this;
    }

    /**
     * Get matches
     *
     * @return array 
     */
    public function getMatches()
    {
        return $this->matches;
    }

    /**
     * Add matches
     *
     * @param \Bundle\TournamentBundle\Entity\FootballMatch $matches
     * @return Round
     */
    public function addMatch(\Bundle\TournamentBundle\Entity\FootballMatch $matches)
    {
        $this->matches[] = $matches;

        return $this;
    }

    /**
     * Remove matches
     *
     * @param \Bundle\TournamentBundle\Entity\FootballMatch $matches
     */
    public function removeMatch(\Bundle\TournamentBundle\Entity\FootballMatch $matches)
    {
        $this->matches->removeElement($matches);
    }

    /**
     * Set tournament
     *
     * @param \Bundle\TournamentBundle\Entity\Tournament $tournament
     * @return Round
     */
    public function setTournament(\Bundle\TournamentBundle\Entity\Tournament $tournament = null)
    {
        $this->tournament = $tournament;

        return $this;
    }

    /**
     * Get tournament
     *
     * @return \Bundle\TournamentBundle\Entity\Tournament 
     */
    public function getTournament()
    {
        return $this->tournament;
    }

    /**
     * Set roundStatus
     *
     * @param \Bundle\TournamentBundle\Entity\RoundStatus $roundStatus
     *
     * @return Round
     */
    public function setStatus(\Bundle\TournamentBundle\Entity\RoundStatus $roundStatus = null)
    {
        $this->roundStatus = $roundStatus;

        return $this;
    }

    /**
     * Get roundStatus
     *
     * @return \Bundle\TournamentBundle\Entity\RoundStatus
     */
    public function getStatus()
    {
        return $this->roundStatus;
    }

    /**
     * Set roundStatus
     *
     * @param \Bundle\TournamentBundle\Entity\RoundStatus $roundStatus
     *
     * @return Round
     */
    public function setRoundStatus(\Bundle\TournamentBundle\Entity\RoundStatus $roundStatus = null)
    {
        $this->roundStatus = $roundStatus;

        return $this;
    }

    /**
     * Get roundStatus
     *
     * @return \Bundle\TournamentBundle\Entity\RoundStatus
     */
    public function getRoundStatus()
    {
        return $this->roundStatus;
    }

}
