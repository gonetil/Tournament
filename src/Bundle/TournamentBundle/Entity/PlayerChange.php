<?php

namespace Bundle\TournamentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PlayerChange
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class PlayerChange
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity="Player")
     * @ORM\JoinColumn(name="player", referencedColumnName="id")
     */
    private $player;

    /**
     * @ORM\ManyToOne(targetEntity="PlayedFootballMatch")
     * @ORM\JoinColumn(name="playedFootballMatch", referencedColumnName="id")
     */
    private $match;
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return PlayerChange
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set player
     *
     * @param \Bundle\TournamentBundle\Entity\Player $player
     *
     * @return PlayerChange
     */
    public function setPlayer(\Bundle\TournamentBundle\Entity\Player $player = null)
    {
        $this->player = $player;

        return $this;
    }

    /**
     * Get player
     *
     * @return \Bundle\TournamentBundle\Entity\Player
     */
    public function getPlayer()
    {
        return $this->player;
    }

    /**
     * Set match
     *
     * @param \Bundle\TournamentBundle\Entity\PlayedFootballMatch $match
     *
     * @return PlayerChange
     */
    public function setMatch(\Bundle\TournamentBundle\Entity\PlayedFootballMatch $match = null)
    {
        $this->match = $match;

        return $this;
    }

    /**
     * Get match
     *
     * @return \Bundle\TournamentBundle\Entity\PlayedFootballMatch
     */
    public function getMatch()
    {
        return $this->match;
    }
}
