<?php

namespace Bundle\TournamentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Referee
 *
 * @ORM\Table(name="referee")
 * @ORM\Entity
 */
class Referee
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100)
     */
    private $name;

    /**
     * @var boolean
     *
     * @ORM\Column(name="enabled", type="boolean")
     */
    private $enabled;

    /**
     * @ORM\OneToMany(targetEntity="FootballMatch", mappedBy="referee")
     * */
    private $participated_matches;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->participated_matches = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return $this->getName();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Referee
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     * @return Referee
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean 
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Add participated_matches
     *
     * @param \Bundle\TournamentBundle\Entity\FootballMatch $participatedMatches
     * @return Referee
     */
    public function addParticipatedMatch(\Bundle\TournamentBundle\Entity\FootballMatch $participatedMatches)
    {
        $this->participated_matches[] = $participatedMatches;

        return $this;
    }

    /**
     * Remove participated_matches
     *
     * @param \Bundle\TournamentBundle\Entity\FootballMatch $participatedMatches
     */
    public function removeParticipatedMatch(\Bundle\TournamentBundle\Entity\FootballMatch $participatedMatches)
    {
        $this->participated_matches->removeElement($participatedMatches);
    }

    /**
     * Get participated_matches
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getParticipatedMatches()
    {
        return $this->participated_matches;
    }

}
