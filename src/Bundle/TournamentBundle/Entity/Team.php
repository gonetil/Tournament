<?php

namespace Bundle\TournamentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Team
 *
 * @ORM\Table(name="team")
 * @ORM\Entity()
 */
class Team
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="flag", type="string", length=255, nullable = true)
     */
    private $flag;

    /**
     * @ORM\OneToMany(targetEntity="TeamInTournament", mappedBy="team")
     */
    private $tournamentParticipations;

    /**
     * @var array
     *
     * @ORM\OneToMany(targetEntity="Application\Sonata\UserBundle\Entity\User", mappedBy="managed_team", cascade={"persist"})
     */
    private $delegates;

    /**
     * @ORM\OneToOne(targetEntity="CurrentAccount", mappedBy="team")
     */
    private $currentAccount;

    public function __toString()
    {
        return $this->getName();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Team
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set flag
     *
     * @param string $flag
     * @return Team
     */
    public function setFlag($flag)
    {
        $this->flag = $flag;

        return $this;
    }

    /**
     * Get flag
     *
     * @return string 
     */
    public function getFlag()
    {
        return $this->flag;
    }

    /**
     * Set delegates
     *
     * @param array $delegates
     * @return Team
     */
    public function setDelegates($delegates)
    {
        foreach ($delegates as $d) {
            $this->addDelegate($d);
        }

        return $this;
    }

    /**
     * Get delegates
     *
     * @return array 
     */
    public function getDelegates()
    {
        return $this->delegates;
    }

    /**
     * Add delegates
     *
     * @param \Application\Sonata\UserBundle\Entity\User $delegates
     * @return Team
     */
    public function addDelegate(\Application\Sonata\UserBundle\Entity\User $delegate)
    {
        $delegate->setManagedTeam($this);
        $this->delegates[] = $delegate;

        return $this;
    }

    /**
     * Remove delegates
     *
     * @param \Application\Sonata\UserBundle\Entity\User $delegates
     */
    public function removeDelegate(\Application\Sonata\UserBundle\Entity\User $delegates)
    {
        $this->delegates->removeElement($delegates);
    }

    /**
     * Add tournamentParticipation
     *
     * @param \Bundle\TournamentBundle\Entity\TeamInTournament $tournamentParticipation
     *
     * @return Team
     */
    public function addTournamentParticipation(\Bundle\TournamentBundle\Entity\TeamInTournament $tournamentParticipation)
    {
        $this->tournamentParticipations[] = $tournamentParticipation;

        return $this;
    }

    /**
     * Remove tournamentParticipation
     *
     * @param \Bundle\TournamentBundle\Entity\TeamInTournament $tournamentParticipation
     */
    public function removeTournamentParticipation(\Bundle\TournamentBundle\Entity\TeamInTournament $tournamentParticipation)
    {
        $this->tournamentParticipations->removeElement($tournamentParticipation);
    }

    /**
     * Get tournamentParticipations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTournamentParticipations()
    {
        return $this->tournamentParticipations;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->tournamentParticipations = new \Doctrine\Common\Collections\ArrayCollection();
        $this->delegates = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set currentAccount
     *
     * @param \Bundle\TournamentBundle\Entity\CurrentAccount $currentAccount
     *
     * @return Team
     */
    public function setCurrentAccount(\Bundle\TournamentBundle\Entity\CurrentAccount $currentAccount = null)
    {
        $this->currentAccount = $currentAccount;

        return $this;
    }

    /**
     * Get currentAccount
     *
     * @return \Bundle\TournamentBundle\Entity\CurrentAccount
     */
    public function getCurrentAccount()
    {
        return $this->currentAccount;
    }

}
