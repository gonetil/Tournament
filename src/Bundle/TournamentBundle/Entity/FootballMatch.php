<?php

namespace Bundle\TournamentBundle\Entity;

use Bundle\TournamentBundle\Common\MatchUtils;
use Doctrine\ORM\Mapping as ORM;

/**
 * Match
 *
 * @ORM\Table(name="football_match")
 * @ORM\Entity
 */
class FootballMatch
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="TeamInTournament", inversedBy="local_matches", cascade={"persist"})
     * @ORM\JoinColumn(name="local_id", referencedColumnName="id")
     */
    private $local;

    /**
     * @ORM\ManyToOne(targetEntity="TeamInTournament", inversedBy="visitor_matches", cascade={"persist"})
     * @ORM\JoinColumn(name="visitor_id", referencedColumnName="id")
     */
    private $visitor;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Round", inversedBy="matches")
     */
    private $round;

    /**
     * @var integer
     *
     * @ORM\Column(name="orderInRound", type="integer")
     */
    private $orderInRound;

    /**
     *  @ORM\ManyToOne(targetEntity="Court", inversedBy="played_matches")
     *
     */
    private $court;

    /**
     * @ORM\ManyToOne(targetEntity="Referee", inversedBy="participated_matches")
     */
    private $referee;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateAndTime", type="datetime", nullable=true)
     */
    private $dateAndTime;

    /**
     * @var string
     *
     * @ORM\Column(name="refereeObservations", type="text", nullable=true)
     */
    private $refereeObservations;

    /**
     *
     * @ORM\ManyToOne(targetEntity="FootballMatchStatus")
     */
    private $status;

    /**
     * @ORM\OneToOne(targetEntity="TeamMatchParticipation", cascade={"persist", "refresh"})
     * @ORM\JoinColumn(name="local_participation", referencedColumnName="id")
     */
    private $local_participation;

    /**
     * @ORM\OneToOne(targetEntity="TeamMatchParticipation", cascade={"persist", "refresh"})
     * @ORM\JoinColumn(name="visitor_participation", referencedColumnName="id")
     */
    private $visitor_participation;

    /**
     * @var integer
     * @ORM\Column(name="visitor_goals", type="integer")
     */
    private $visitor_goals = -1;

    /**
     * @var integer
     * @ORM\Column(name="local_goals", type="integer")
     */
    private $local_goals = -1;

    /**
     * @ORM\OneToOne(targetEntity="PlayedFootballMatch", cascade={"persist", "refresh"})
     * @ORM\JoinColumn(name="played_football_match", referencedColumnName="id")
     */
    private $playedFootballMatch;

    /**
     * Constructor
     */
    public function __construct()
    {
        
    }

    public function __toString()
    {

        $tournament = (($round = $this->getRound()) && ($round->getTournament())) ? " (" . $round->getTournament()->getName() . ")" : "";
        return $this->getLocal() . " vs " . $this->getVisitor() . $tournament;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set local
     * @return Match
     */
    public function setLocal($local)
    {
        $this->local = $local;

        return $this;
    }

    /**
     * Get local
     *
     * @return integer 
     */
    public function getLocal()
    {
        return $this->local;
    }

    /**
     * Set visitor
     *
     * @param integer $visitor
     * @return Match
     */
    public function setVisitor($visitor)
    {
        $this->visitor = $visitor;

        return $this;
    }

    /**
     * Get visitor
     *
     * @return integer 
     */
    public function getVisitor()
    {
        return $this->visitor;
    }

    /**
     * Set round
     *
     * @param integer $round
     * @return Match
     */
    public function setRound($round)
    {
        $this->round = $round;

        return $this;
    }

    /**
     * Get round
     *
     * @return integer 
     */
    public function getRound()
    {
        return $this->round;
    }

    /**
     * Set orderInRound
     *
     * @param integer $orderInRound
     * @return Match
     */
    public function setOrderInRound($orderInRound)
    {
        $this->orderInRound = $orderInRound;

        return $this;
    }

    /**
     * Get orderInRound
     *
     * @return integer 
     */
    public function getOrderInRound()
    {
        return $this->orderInRound;
    }

    /**
     * Set dateAndTime
     *
     * @param \DateTime $dateAndTime
     * @return Match
     */
    public function setDateAndTime($dateAndTime)
    {
        $this->dateAndTime = $dateAndTime;

        return $this;
    }

    /**
     * Get dateAndTime
     *
     * @return \DateTime 
     */
    public function getDateAndTime()
    {
        return $this->dateAndTime;
    }

    /**
     * Set refereeObservations
     *
     * @param string $refereeObservations
     * @return Match
     */
    public function setRefereeObservations($refereeObservations)
    {
        $this->refereeObservations = $refereeObservations;

        return $this;
    }

    /**
     * Get refereeObservations
     *
     * @return string 
     */
    public function getRefereeObservations()
    {
        return $this->refereeObservations;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Match
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Add local
     *
     * @param \Bundle\TournamentBundle\Entity\Team $local
     * @return Match
     */
    public function addLocal(\Bundle\TournamentBundle\Entity\Team $local)
    {
        $this->local[] = $local;

        return $this;
    }

    /**
     * Remove local
     *
     * @param \Bundle\TournamentBundle\Entity\Team $local
     */
    public function removeLocal(\Bundle\TournamentBundle\Entity\Team $local)
    {
        $this->local->removeElement($local);
    }

    /**
     * Add visitor
     *
     * @param \Bundle\TournamentBundle\Entity\Team $visitor
     * @return Match
     */
    public function addVisitor(\Bundle\TournamentBundle\Entity\Team $visitor)
    {
        $this->visitor[] = $visitor;

        return $this;
    }

    /**
     * Remove visitor
     *
     * @param \Bundle\TournamentBundle\Entity\Team $visitor
     */
    public function removeVisitor(\Bundle\TournamentBundle\Entity\Team $visitor)
    {
        $this->visitor->removeElement($visitor);
    }

    /**
     * sets court
     *
     * @param \Bundle\TournamentBundle\Entity\Court $court
     * @return Match
     */
    public function setCourt(\Bundle\TournamentBundle\Entity\Court $court)
    {
        $this->court = $court;

        return $this;
    }

    /**
     * gets
     *
     * @return Court
     */
    public function getCourt()
    {
        return $this->court;
    }

    /**
     * Add refereee
     *
     * @param \Bundle\TournamentBundle\Entity\Referee $refereee
     * @return Match
     */
    public function addRefereee(\Bundle\TournamentBundle\Entity\Referee $refereee)
    {
        $this->refereee[] = $refereee;

        return $this;
    }

    /**
     * Remove refereee
     *
     * @param \Bundle\TournamentBundle\Entity\Referee $refereee
     */
    public function removeRefereee(\Bundle\TournamentBundle\Entity\Referee $refereee)
    {
        $this->refereee->removeElement($refereee);
    }

    /**
     * Get referee
     *
     * @return \Bundle\TournamentBundle\Entity\Referee 
     */
    public function getReferee()
    {
        return $this->referee;
    }

    /**
     * Set referee
     *
     * @param \Bundle\TournamentBundle\Entity\Referee $referee
     * @return FootballMatch
     */
    public function setReferee(\Bundle\TournamentBundle\Entity\Referee $referee = null)
    {
        $this->referee = $referee;

        return $this;
    }

    /**
     * Set local_participation
     *
     * @param \Bundle\TournamentBundle\Entity\TeamMatchParticipation $localParticipation
     * @return FootballMatch
     */
    public function setLocalParticipation(\Bundle\TournamentBundle\Entity\TeamMatchParticipation $localParticipation = null)
    {
        $this->local_participation = $localParticipation;

        return $this;
    }

    /**
     * Get local_participation
     *
     * @return \Bundle\TournamentBundle\Entity\TeamMatchParticipation 
     */
    public function getLocalParticipation()
    {
        return $this->local_participation;
    }

    /**
     * Set visitor_participation
     *
     * @param \Bundle\TournamentBundle\Entity\TeamMatchParticipation $visitorParticipation
     * @return FootballMatch
     */
    public function setVisitorParticipation(\Bundle\TournamentBundle\Entity\TeamMatchParticipation $visitorParticipation = null)
    {
        $this->visitor_participation = $visitorParticipation;

        return $this;
    }

    /**
     * Get visitor_participation
     *
     * @return \Bundle\TournamentBundle\Entity\TeamMatchParticipation 
     */
    public function getVisitorParticipation()
    {
        return $this->visitor_participation;
    }

    /**
     * Set visitor_goals
     *
     * @param integer $visitorGoals
     * @return FootballMatch
     */
    public function setVisitorGoals($visitorGoals)
    {
        $this->visitor_goals = $visitorGoals;

        return $this;
    }

    /**
     * Get visitor_goals
     *
     * @return integer 
     */
    public function getVisitorGoals()
    {
        return $this->visitor_goals;
    }

    /**
     * Set local_goals
     *
     * @param integer $localGoals
     * @return FootballMatch
     */
    public function setLocalGoals($localGoals)
    {
        $this->local_goals = $localGoals;

        return $this;
    }

    /**
     * Get local_goals
     *
     * @return integer 
     */
    public function getLocalGoals()
    {
        return $this->local_goals;
    }

    /**
     * returns the team that won the football match
     * @return Team
     */
    public function getWinner()
    {
        $winner = null;
        if (MatchUtils::isMatchEnded($this)) {
            if ($this->getLocalParticipation()->getGoalsFor() > $this->getVisitorParticipation()->getGoalsFor())
                $winner = $this->getLocalParticipation()->getTeam();
            elseif ($this->getLocalParticipation()->getGoalsFor() < $this->getVisitorParticipation()->getGoalsFor())
                $winner = $this->getVisitorParticipation()->getTeam();
            else
                $winner = FALSE; //ain't no winner, it's a tie
        }
        return $winner;
    }

    /**
     * returns the team that lost the football match
     * @return Team
     */
    public function getLooser()
    {
        $looser = null;
        if (MatchUtils::isMatchEnded($this)) {
            if ($this->getLocalParticipation()->getGoalsFor() < $this->getVisitorParticipation()->getGoalsFor())
                $looser = $this->getLocalParticipation()->getTeam();
            elseif ($this->getLocalParticipation()->getGoalsFor() > $this->getVisitorParticipation()->getGoalsFor())
                $looser = $this->getVisitorParticipation()->getTeam();
            else
                $looser = FALSE; //ain't no looser, it's a tie
        }
        return $looser;
    }

    /**
     * Set playedFootballMatch
     *
     * @param \Bundle\TournamentBundle\Entity\PlayedFootballMatch $playedFootballMatch
     *
     * @return FootballMatch
     */
    public function setPlayedFootballMatch(\Bundle\TournamentBundle\Entity\PlayedFootballMatch $playedFootballMatch = null)
    {
        $this->playedFootballMatch = $playedFootballMatch;

        return $this;
    }

    /**
     * Get playedFootballMatch
     *
     * @return \Bundle\TournamentBundle\Entity\PlayedFootballMatch
     */
    public function getPlayedFootballMatch()
    {
        return $this->playedFootballMatch;
    }

}
