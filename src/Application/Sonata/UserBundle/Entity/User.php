<?php

namespace Application\Sonata\UserBundle\Entity;

use Sonata\UserBundle\Entity\BaseUser as BaseUser;

class User extends BaseUser
{

    /**
     * @var integer $id
     */
    protected $id;
    
    protected $managed_team;

    /**
     * Get id
     *
     * @return integer $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set managedTeam
     *
     * @param \Bundle\TournamentBundle\Entity\Team $managedTeam
     *
     * @return User
     */
    public function setManagedTeam(\Bundle\TournamentBundle\Entity\Team $managedTeam = null)
    {
        $this->managed_team = $managedTeam;

        return $this;
    }

    /**
     * Get managedTeam
     *
     * @return \Bundle\TournamentBundle\Entity\Team
     */
    public function getManagedTeam()
    {
        return $this->managed_team;
    }

}
